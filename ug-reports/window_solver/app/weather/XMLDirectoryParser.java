package org.grailrtls.app.weather;

import java.io.IOException;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Parses the online XML directory to a hash table
 * 
 * @author Jonathan
 * 
 */
public class XMLDirectoryParser
{
    private static Document dom;

    public static Hashtable<String, Person> getPersons(String file)
    {
	parseXmlFile(file);
	return parseDocument();
    }

    private static void parseXmlFile(String file)
    {
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	try
	{
	    DocumentBuilder db = dbf.newDocumentBuilder();
	    dom = db.parse(file);

	} catch (ParserConfigurationException pce)
	{
	    pce.printStackTrace();
	} catch (SAXException se)
	{
	    se.printStackTrace();
	} catch (IOException ioe)
	{
	    ioe.printStackTrace();
	}
    }

    private static Hashtable<String, Person> parseDocument()
    {

	Hashtable<String, Person> persons = new Hashtable<String, Person>();

	Element docEle = dom.getDocumentElement();
	NodeList nl = docEle.getElementsByTagName("person");
	if (nl != null && nl.getLength() > 0)
	{
	    for (int i = 0; i < nl.getLength(); i++)
	    {
		Element el = (Element) nl.item(i);
		Person p = getPerson(el);
		if (p.getRoom() != null)
		    persons.put(p.getRoom(), p);
	    }
	}
	return persons;
    }

    private static Person getPerson(Element e)
    {

	String name = getTextValue(e, "name");
	String phoneNumber = getTextValue(e, "phone");
	String room = getTextValue(e, "room");
	String email = getTextValue(e, "email");

	Person p = new Person(name, phoneNumber, room, email);

	return p;
    }

    /**
     * Gets the value associated with a tag.
     * 
     * @param ele
     * @param tagName
     * @return
     */
    private static String getTextValue(Element ele, String tagName)
    {
	String textVal = null;
	NodeList nl = ele.getElementsByTagName(tagName);
	if (nl != null && nl.getLength() > 0)
	{
	    Element el = (Element) nl.item(0);
	    textVal = el.getFirstChild().getNodeValue();
	}

	return textVal;
    }

}
