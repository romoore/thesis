package org.grailrtls.app.weather;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * This class handles sending email to people
 * 
 * @author Jonathan
 * 
 */
public class Emailer
{
    /**
     * Sends email to recipients from grail.notifier@gmail.com
     * 
     * @param recipients
     *            Array of recipients' emails
     * @param subject
     *            Subject for email
     * @param msg
     *            Email message
     * @return True if message sent, false if not
     */
    public static boolean sendNotification(String[] recipients, String subject, String msg)
    {
	System.out.println("Sending email...");
	try
	{
	    String host = "smtp.gmail.com";
	    String from = "grail.notifier";
	    Properties props = System.getProperties();
	    props.put("mail.smtp.host", host);
	    props.put("mail.smtp.user", from);
	    props.put("mail.smtp.password", "Grail335");
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.auth", "true");

	    Session session = Session.getDefaultInstance(props, null);
	    MimeMessage message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(from));
	    for (String s : recipients)
	    {
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(s));
	    }
	    message.setSubject(subject);
	    message.setText(msg);

	    Transport transport = session.getTransport("smtps");
	    transport.connect(host, from, "Grail335");
	    transport.sendMessage(message, message.getAllRecipients());
	    transport.close();
	    return true;
	} catch (Exception e)
	{
	    e.printStackTrace();
	    return false;
	}
    }
}