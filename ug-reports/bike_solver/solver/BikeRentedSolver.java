package org.grailrtls.solver;

import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import org.grailrtls.libworldmodel.client.ClientWorldConnection;
import org.grailrtls.libworldmodel.client.Response;
import org.grailrtls.libworldmodel.client.StepResponse;
import org.grailrtls.libworldmodel.client.WorldState;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.solver.listeners.DataListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class updates the rental status of bikes in the world model based on their location and the rental office location.
 * 
 * @author Jonathan
 * 
 */
public class BikeRentedSolver extends Thread
{
    private static final Logger log = LoggerFactory.getLogger(BikeRentedSolver.class);

    private final String RENTALOFFICE_ATTRIBUTE = "rentaloffice";
    private final String RENTALOFFICE_LOCATION_ATTRIBUTE = "location";
    private final String BIKE_LOCATION_ATTRIBUTE = "location";
	private final String BIKE_RENTED_ATTRIBUTE = "rented";
    private final String ORIGIN = "bike.rented.solver";
    private final String RENTED_NO = "No";
    private final String RENTED_YES = "Yes";

    private ClientWorldConnection wmc;
    private static String wmHost;
    private static int wmPort = 7012;
    private static boolean debug = false;

    private Hashtable<String, String> bikes = new Hashtable<String, String>();
    private Hashtable<String, String> bike_hash = new Hashtable<String, String>();
    private Hashtable<String, BikeReaderThread> bike_reader_threads = new Hashtable<String, BikeReaderThread>();

    public static void main(String[] args)
    {
	if (args.length < 2)
	{
	    System.err.println("I need at least 2 things: <World Model Host> <World Model Port>");
	    return;
	}
	else
	{
	    for (String s : args)
	    {
		if ("d".equalsIgnoreCase(s))
		    debug = true;
	    }
	}

	String host = args[0];
	wmHost = args[0];
	int port = Integer.parseInt(args[1]);

	BikeRentedSolver brs = new BikeRentedSolver(host, port);
	brs.start();
    }

    public BikeRentedSolver(final String host, final int port)
    {
	wmc = new ClientWorldConnection();
	wmc.setHost(host);
	wmc.setPort(port);
	if (!wmc.connect())
	{
	    System.err.println("Couldn't connect to the world model!  Check your connection parameters.");
	    return;
	}
    }

    public void run()
    {
	long now = System.currentTimeMillis();
	long period = 5 * 60 * 1000; // 5 minutes
	System.out.println("Requesting from " + new Date(now) + " every " + period + " ms.");
	StepResponse response = wmc.getStreamRequest(".*bike.*", now, period, ".*");
	WorldState state = null;

	while (!response.isComplete())
	{
	    try
	    {
		state = response.next();
	    } catch (Exception e)
	    {
		System.err.println("Error occured during request: " + e);
		e.printStackTrace();
		break;
	    }
	    Collection<String> uris = state.getURIs();
	    if (uris != null)
	    {
		for (String uri : uris)
		{
		    Collection<Attribute> attribs = state.getState(uri);
		    for (Attribute att : attribs)
		    {
			if (RENTALOFFICE_ATTRIBUTE.equals(att.getAttributeName()))
			{
			    try
			    {
				String rentalOffice = new String(att.getData(), "UTF-16BE");
				bikes.put(uri, rentalOffice);
			    } catch (Exception e)
			    {
				dprint("Error parsing byte data");
			    }
			    break;
			}
		    }
		}
	    }
	}

	// Remove old bikes or changed bikes
	Enumeration<String> e = bike_hash.keys();
	while (e.hasMoreElements())
	{
	    String key = (String) e.nextElement();
	    if (bikes.get(key) != bike_hash.get(key))
	    {
		bike_reader_threads.get(key).interrupt();
		bike_reader_threads.remove(key);

		bike_hash.remove(key);
	    }
	}

	// Add new bikes
	e = bikes.keys();
	while (e.hasMoreElements())
	{
	    String key = (String) e.nextElement();
	    if (!bike_hash.containsKey(key))
	    {
		BikeReaderThread brThread = new BikeReaderThread(key, bikes.get(key));
		brThread.start();
		bike_reader_threads.put(key, brThread);

		bike_hash.put(key, bikes.get(key));
	    }
	}

	wmc.disconnect();
    }

    /**
     * Prints debugging messages
     * 
     * @param s
     *            Input string
     */
    public void dprint(String s)
    {
	if (debug)
	    System.out.println("[DEBUG] " + s);
    }

    public class BikeReaderThread extends Thread implements ConnectionListener, DataListener
    {
	private boolean wmReadyForSolutions = false;
	private String bikeUri;
	private String rentalOfficeUri;
	private final SolverWorldModelInterface worldModel = new SolverWorldModelInterface();
	private String bike_location;

	/**
	 * BikeReaderThread constructor
	 * 
	 * @param bikeUri
	 *            URI of the bike
	 * @param rentalOfficeUri
	 *            URI of the bike's rental office
	 */
	public BikeReaderThread(final String bikeUri, final String rentalOfficeUri)
	{
	    this.bikeUri = bikeUri;
	    this.rentalOfficeUri = rentalOfficeUri;

	    this.worldModel.setHost(wmHost);
	    this.worldModel.setPort(wmPort);

	    this.worldModel.addConnectionListener(this);
	    this.worldModel.addDataListener(this);
	    TypeSpecification testType = new TypeSpecification();
	    testType.setIsTransient(false);
	    testType.setUriName(BIKE_LOCATION_ATTRIBUTE);
	    this.worldModel.addType(testType);
	    this.worldModel.setOriginString(ORIGIN);
	    this.worldModel.setCreateUris(true);
	    this.worldModel.setDisconnectOnException(true);
	    this.worldModel.setStayConnected(true);
	}

	public void run()
	{
	    if (!this.worldModel.doConnectionSetup())
	    {
		System.out.println("Unable to establish a connection to the world model.");
		System.exit(1);
	    }
	    System.out.println("Connected to world model at " + this.worldModel.getHost() + ":" + this.worldModel.getPort());

	    StepResponse response = wmc.getStreamRequest(bikeUri, System.currentTimeMillis(), 100l, ".*");
	    WorldState state = null;

	    while (!response.isComplete())
	    {
		try
		{
		    state = response.next();
		} catch (Exception e)
		{
		    System.err.println("Error occured during request: " + e);
		    e.printStackTrace();
		    break;
		}
		Collection<String> uris = state.getURIs();
		if (uris != null)
		{
		    for (String uri : uris)
		    {
			Collection<Attribute> attribs = state.getState(uri);
			for (Attribute att : attribs)
			{
			    if (BIKE_LOCATION_ATTRIBUTE.equals(att.getAttributeName()))
			    {
				try
				{
				    bike_location = new String(att.getData(), "UTF-16BE");
				    sendRentedSolution(bike_location);
				} catch (Exception e)
				{
				    dprint(e.getMessage());
				}
				break;
			    }
			}
		    }
		}
	    }
	}

	/**
	 * Sends "rented" solution to the world model for the bike depending on whether it is at its rental office or not.
	 * 
	 * @param bikeLocation
	 *            Location of the bike.
	 * @throws Exception
	 *             Throws an exception if retrieval of the world model information fails.
	 */
	private void sendRentedSolution(String bikeLocation) throws Exception
	{
	    String rentaloffice_location;
	    long now = System.currentTimeMillis();
	    Response response = wmc.getSnapshot(rentalOfficeUri, now, now, ".*");
	    WorldState state = response.get();
	    for (String uri : state.getURIs())
	    {
		for (Attribute att : state.getState(uri))
		{
		    if (RENTALOFFICE_LOCATION_ATTRIBUTE.equals(att.getAttributeName()))
		    {
			rentaloffice_location = new String(att.getData(), "UTF-16BE");
			if (bike_location.equals(rentaloffice_location))
			{
			    sendSolution(bikeUri, RENTED_NO.getBytes("UTF-16BE"), System.currentTimeMillis());
			    dprint("Solution sent: " + bikeUri + ", " + RENTED_NO);
			}
			else
			{
			    sendSolution(bikeUri, RENTED_YES.getBytes("UTF-16BE"), System.currentTimeMillis());
			    dprint("Solution sent: " + bikeUri + ", " + RENTED_YES);
			}
		    }
		}
	    }
	}

	/**
	 * Sends a solution to the URI
	 * 
	 * @param targetUri
	 *            URI of object in the world model
	 * @param data
	 *            Byte array of data
	 * @param time
	 *            Creation time
	 */
	public void sendSolution(String targetUri, byte[] data, long time)
	{
	    Solution solution = new Solution();
	    solution.setTargetName(targetUri);
	    solution.setAttributeName(BIKE_RENTED_ATTRIBUTE);
	    solution.setTime(time);
	    solution.setData(data);
	    if (this.wmReadyForSolutions)
	    {
		this.worldModel.sendSolution(solution);
	    }
	}

	@Override
	public void startTransientReceived(SolverWorldModelInterface worldModel,
		StartTransientMessage message)
	{

	}

	@Override
	public void stopTransientReceived(SolverWorldModelInterface worldModel,
		StopTransientMessage message)
	{

	}

	@Override
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel,
		TypeAnnounceMessage message)
	{
	    log.info("Announced type specifications. Ready to send solutions.");
	    this.wmReadyForSolutions = true;
	}

	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel)
	{
	    log.warn("Temporarily lost connection to {}", worldModel);
	    this.wmReadyForSolutions = false;

	}

	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel)
	{
	    log.error("Permanently lost connection to {}", worldModel);
	    System.exit(1);

	}

	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel)
	{
	    log.info("Connected to {}", worldModel);
	}
    }
}
