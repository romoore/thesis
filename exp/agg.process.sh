#!/bin/bash

# Process aggregator log files
./parse.20140730.pl *.log

# Extract service rate vs processing time/lifetime data
./agg.rate_vs.pl agg1-[0-9]*.csv
mv rate_vs.csv agg1.rate_vs.csv

./agg.rate_vs.pl agg2-3-[0-9]*.csv
mv rate_vs.csv agg2-3.rate_vs.csv
./agg.rate_vs.pl agg2-6-[0-9]*.csv
mv rate_vs.csv agg2-6.rate_vs.csv
./agg.rate_vs.pl agg2-9-[0-9]*.csv
mv rate_vs.csv agg2-9.rate_vs.csv

./agg.rate_vs.pl agg3-1-[0-9]*.csv
mv rate_vs.csv agg3-1.rate_vs.csv
./agg.rate_vs.pl agg3-3-[0-9]*.csv
mv rate_vs.csv agg3-3.rate_vs.csv
./agg.rate_vs.pl agg3-6-[0-9]*.csv
mv rate_vs.csv agg3-6.rate_vs.csv
./agg.rate_vs.pl agg3-9-[0-9]*.csv
mv rate_vs.csv agg3-9.rate_vs.csv

./agg.rate_vs.pl agg4-1-[0-9]*.csv
mv rate_vs.csv agg4-1.rate_vs.csv
./agg.rate_vs.pl agg4-3-[0-9]*.csv
mv rate_vs.csv agg4-3.rate_vs.csv
./agg.rate_vs.pl agg4-6-[0-9]*.csv
mv rate_vs.csv agg4-6.rate_vs.csv
./agg.rate_vs.pl agg4-9-[0-9]*.csv
mv rate_vs.csv agg4-9.rate_vs.csv

# Generate EPS files
./agg.plot.agg1.gp
./agg.plot.agg2.gp
./agg.plot.agg3.gp
./agg.plot.agg4.gp
