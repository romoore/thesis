#!/usr/bin/perl

use strict;
use warnings;
use DateTime::Format::Strptime;

my %rate_vs = ();

foreach my $fn (@ARGV) {
  $fn =~ s/(.+)\.[^.]+$/$fn/;


  my $basename = $1; # base filename for adding extensions

  
  open my $file, $fn or die "Could not open $fn: $!"; # Try to open each file
  while (my $line = <$file>){
    # 30,7711.6,0.590011,436
    if( $line =~ /^.*,([0-9]+\.[0-9]+),([0-9]+\.[0-9]+),([0-9]+)$/ ){
      my $rate = int(1000000/$1);
      my $proc = $2;
      my $life = $3;


      if(not exists $rate_vs{$rate}){
        @{$rate_vs{$rate}} = ($proc,$life);
      }
      else {
        push @{$rate_vs{$rate}}, ($proc,$life);
      }
    }


  }
  close $file;

}

open (OUTFILE, '>rate_vs.csv');
foreach my $rate (sort {$a <=> $b} (keys(%rate_vs))) {
  my @aoa = @{$rate_vs{$rate}};
  my $avg_proc = 0;
  my $avg_life = 0;
  my $cnt = 0;
  # Calculate mean
  for (my $i = 0; $i < $#aoa; $i += 2){
    $avg_proc += $aoa[$i];
    $avg_life += $aoa[$i+1];
    $cnt += 1;
  }
  $avg_proc /= $cnt;
  $avg_life /= $cnt;

  # Calculate variance
  my $proc_var1 = 0;
  my $proc_var2 = 0;
  my $life_var1 = 0;
  my $life_var2 = 0;
  while(@aoa){
    my $p = shift @aoa;
    my $l = shift @aoa;
    $proc_var1 += ($p - $avg_proc)**2;
    $proc_var2 += ($p - $avg_proc);

    $life_var1 += ($l - $avg_life)**2;
    $life_var2 += ($l - $avg_life);
  }

  my $proc_var = ($proc_var1 - ($proc_var2**2)/$cnt)/($cnt);
  my $life_var = ($life_var1 - ($life_var2**2)/$cnt)/($cnt);

  my $proc_err = sqrt($proc_var) / sqrt($cnt);
  my $life_err = sqrt($life_var) / sqrt($cnt);
  print OUTFILE "$rate,$avg_proc,$proc_err,$avg_life,$life_err\n";
}
close (OUTFILE)


