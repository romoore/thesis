Insert 100K, 1M, 10M, 100M (Expired/Unexpired)
	All at once --createAttributes
	Delete one at a time -> at the end
	One at a time --individual, then exit
	
	Need to have a 2nd app running to request those values
	Measure with and without 2nd app
	

Perform Range query
	Over entire data set
	Over 10% at a time
	Over 5% at a time
	Over 1% at a time

Perform Snapshot query
	At 19 points within data set


Perform ID search
	Simple ".*"
	Medium "i_[0-9]{3}"
	Complex catastrophic backtracking
		/i_(([^\\"]*))*"/  ---- No matches for i_000000X

	search id.*
	search ^(.*?[a-z])(.*[0-9]){8}9 = over 1ms
	search (([^\.]*))*p             = over 10ms (empty)
	search (([^\.]*))*1		= over 5ms (matches!)
