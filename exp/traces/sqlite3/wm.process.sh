#!/bin/bash

find insert -iname "worldmodel.log" | xargs ./wm.parseInsert.pl
find insert -iname "worldmodel.log" | xargs ./wm.cdf.pl
find *expire -iname "*.log" | xargs ./wm.parseLogs.pl
./wm.plot.gp
