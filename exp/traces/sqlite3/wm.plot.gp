#!/usr/bin/gnuplot
#set bmargin 2.5;

set datafile sep ','; 

## Slides

set xlabel 'World Model Size (Attributes)' font "Liberation Sans,32"
set xlabel offset 0,-5
set xtics rotate font "Liberation Sans,32"
set ytics font "Liberation Sans,32"
set bmargin 13 
set ylabel "Time ({/Symbol m}s)" offset -8,0 font "Liberation Sans,32"
set lmargin 20
set tmargin 5 
set key inside bottom right box width 28 height 2 enhanced spacing 5 font "Liberation Sans,34"
set terminal pngcairo size 1440,1440 enhanced
set title "World Model: Regex Search" offset 0,1 font "Liberation Sans,34"
set grid xtics ytics lc rgb '#333333' lw 2

set log y

set output "both.wm.regex2.png";
#set yrange [900:2.2e6];
set yrange [*:*];
set xrange [*:*];
set xrange [900:2.2e6];
plot 'worldmodel-regex2-boxes.csv' u ($1*1.05):($2+$3):4:5:($2-$3) title 'SQLite World Model' w candlesticks whiskerbars lc 1 lw 4,\
     'worldmodel-regex2-boxes.csv' u 1:6 title 'SQLite 99 Percentile' w linespoints lc 1 lw 4,\
     'mysql-worldmodel-regex2-boxes.csv' u ($1*.95):($2+$3):4:5:($2-$3) title 'MySQL World Model' w candlesticks whiskerbars lc 3 lw 4,\
     'mysql-worldmodel-regex2-boxes.csv' u 1:6 title 'MySQL 99 Percentile' w linespoints lc 3 lw 4
#     'worldmodel-regex2-boxes.csv' u ($1*1.12):($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2 lw 4,\
#     'worldmodel-regex2-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2 lw 4,\
#     'mysql-worldmodel-regex2-boxes.csv' u ($1*.89):($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 4 lw 4,\
#     'mysql-worldmodel-regex2-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 4 lw 4

set title "World Model Performance: 5% Range Request"
set output "both.wm.range05.png";
#set key inside right bottom  box width -1 height 2 enhanced spacing 1
set yrange [1000:1.5e6]; 
set xrange [900:2.2e6];
set log xy
plot 'worldmodel-range05-boxes.csv' u 1:($2+$3):4:5:($2-$3) title 'SQLite World Model' w candlesticks whiskerbars lc 1 lw 4,\
     'worldmodel-range05-boxes.csv' u 1:6 title 'SQLite 99 Percentile' w linespoints lc 1 lw 4,\
     'mysql-worldmodel-range05-boxes.csv' u 1:($2+$3):4:5:($2-$3) title 'MySQL World Model' w candlesticks whiskerbars lc 2 lw 4,\
     'mysql-worldmodel-range05-boxes.csv' u 1:6 title 'MySQL 99 Percentile' w linespoints lc 2 lw 4

