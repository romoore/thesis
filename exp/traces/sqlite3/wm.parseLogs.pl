#!/usr/bin/perl

use strict;
use warnings;
use DateTime::Format::Strptime;
use File::Basename;
use POSIX qw/ceil/;

# Calculate mean and StdDev
# Inputs: Hash value by size
# Returns: (Hash by size of (mean, stderr, -stddev, +stddev, 95%)
sub meanAndStdDev {
  my ($valueHoA) = @_;
  my %retHoA = ();
  
  # Go through each key (set size) and compute
  # mean, stddev, min,max
  foreach my $key (keys(%$valueHoA)) {

    my $min = 1e20;
    my $max = 0;
    
    # Calculate mean and StdDev
    my @vals = @{$valueHoA->{$key}};
    my $size = @vals;
    # 99 percentile index
    my $idx99 = ceil($size*.99)-1;
    my $idx95 = ceil($size*.95)-1;
    my @sortVals = sort {$a <=> $b} (@vals);
    # 99 percentile value
    my $val99 = $sortVals[$idx99];
    my $val95 = $sortVals[$idx95];

    my $n = 0;
    my $sum1 = 0;

    foreach my $x (@vals) {
      if($x < $min){
        $min = $x;
      }
      if($x > $max){
        $max = $x;
      }
      $n += 1;
      $sum1 += $x;
    }
    my $mean = $sum1/$n;

    my $sum2 = 0;
    my $sum3 = 0;
    foreach my $x (@vals){
      $sum2 += ($x - $mean)**2;
      $sum3 += ($x - $mean);
    }

    my $stdDev = sqrt(($sum2 - (($sum3**2)/$n))/($n-1));
    my $stdErr = $stdDev/sqrt($n);

    @{$retHoA{$key}} = (int($mean),int($stdErr),int($mean-$stdDev),int($mean+$stdDev),$val99,$val95);
  }

  return \%retHoA;
}

# All data of Range 1% by size
# Will be 100 each based on entire attribute size
my %exp_hoa_all_range1_vs_size = ();
my %noexp_hoa_all_range1_vs_size = ();

# All data of Range 5% by size
my %exp_hoa_all_range5_vs_size = ();
my %noexp_hoa_all_range5_vs_size = ();

# All data of Range 10% by size
my %exp_hoa_all_range10_vs_size = ();
my %noexp_hoa_all_range10_vs_size = ();

# All data of Expire Attribute by size
my %exp_hoa_all_expireA_vs_size = ();

# All data of Delete Attribute by size
my %exp_hoa_all_deleteA_vs_size = ();
my %noexp_hoa_all_deleteA_vs_size = ();

# All data of Regex Searches by size
# Regex 1 = ".*"
# Regex 2 = "^(.*?[a-z])(.*[0-9]){8}9"
# Regex 3 = "(([^\.]*))*1" - has backtracking
# Regex 4 = "(([^\.]*))*p" - backtracking and no matches
my %exp_hoa_all_regex1_vs_size = ();
my %exp_hoa_all_regex2_vs_size = ();
my %exp_hoa_all_regex3_vs_size = ();
my %exp_hoa_all_regex4_vs_size = ();
my %noexp_hoa_all_regex1_vs_size = ();
my %noexp_hoa_all_regex2_vs_size = ();
my %noexp_hoa_all_regex3_vs_size = ();
my %noexp_hoa_all_regex4_vs_size = ();

# All data for Snapshot requests
my %exp_hoa_snap_vs_size = ();
my %noexp_hoa_snap_vs_size = ();

my %exp_size_vs_insert = ();
my %noexp_size_vs_insert = ();

my %exp_req_all = ();
my %noexp_req_all = ();

my %exp_req_10m = ();
my %exp_req_10c = ();
my %noexp_req_10m = ();
my %noexp_req_10c = ();

my %exp_req_5m = ();
my %exp_req_5c = ();
my %noexp_req_5m = ();
my %noexp_req_5c = ();

my %exp_req_1m = ();
my %exp_req_1c = ();
my %noexp_req_1m = ();
my %noexp_req_1c = ();

print "Generating World Model query CSVs\n";
# Enable buffer autoflush
$| = 1;

my $numFile = scalar @ARGV;
my $fileIndex = 0;

foreach my $fn (@ARGV) {

  print "\r[";
  my $frac = ($fileIndex+1)/$numFile;
  for(my $i = 0; $i < 40; $i += 1){
    print (($i*.025) <= $frac ? '#' : ' ');
  }
  print '] ('.int($frac*100).'%)';
  $fileIndex += 1;

  $fn =~ s/(.+)\.[^.]+$/$fn/;

  open my $file, $fn or die "Could not open $fn: $!"; # Try to open each file
#  $fn =~ /.*\.([0-9]{4}\-[0-9]{2}\-[0-9]{2})/; 
  my ($filename,$path) = fileparse($fn);
  $path =~ /([a-zA-Z0-9_]+)\/$/;
  my $savename = $1;

  $savename =~ /([0-9]+)([km])/;
  my $size = $1;
  my $unit = $2;
  my $sizeKey = ($unit eq 'k' ? $size*1000 : $size*1000000);
  my $rangeCount = 0;
  my $regexCount = 0; # 4 regexes, 10 each, 40 total
  my $snapshotCount = 0; # 2.5 -> 97.5, step 5. Total 19

  while (my $line = <$file>){
    if($filename =~ /worldmodel\.log$/){
      # Insert timing for world model
      # server/Insert/1: 258512
      if( $line =~ /^server\/Insert\/([0-9]+): ([0-9]+)$/ ){
        my $count = $1;
        my $time = int($2/1000);
        if($path =~ /noexpire/){
          $noexp_size_vs_insert{$count} = $time;
        }else {
          $exp_size_vs_insert{$count} = $time;
        }
        next;
      }
      # Regex search time
      if ($line =~ /^core\/Search: ([0-9]+)$/ ){
        my $time = int($1/1000);
        if($path =~ /noexpire/){
          if($regexCount <= 10){
            if(not exists $noexp_hoa_all_regex1_vs_size{$sizeKey}){
              @{$noexp_hoa_all_regex1_vs_size{$sizeKey}} = ();
            }
            push @{$noexp_hoa_all_regex1_vs_size{$sizeKey}}, $time;
          }elsif($regexCount <= 20){
            if(not exists $noexp_hoa_all_regex2_vs_size{$sizeKey}){
              @{$noexp_hoa_all_regex2_vs_size{$sizeKey}} = ();
            }
            push @{$noexp_hoa_all_regex2_vs_size{$sizeKey}}, $time;
          }elsif($regexCount <= 30){
            if(not exists $noexp_hoa_all_regex3_vs_size{$sizeKey}){
              @{$noexp_hoa_all_regex3_vs_size{$sizeKey}} = ();
            }
            push @{$noexp_hoa_all_regex3_vs_size{$sizeKey}}, $time;
          }elsif($regexCount <= 40){
            if(not exists $noexp_hoa_all_regex4_vs_size{$sizeKey}){
              @{$noexp_hoa_all_regex4_vs_size{$sizeKey}} = ();
            }
            push @{$noexp_hoa_all_regex4_vs_size{$sizeKey}}, $time;
          }
        }else {
          if($regexCount <= 10){
            if(not exists $exp_hoa_all_regex1_vs_size{$sizeKey}){
              @{$exp_hoa_all_regex1_vs_size{$sizeKey}} = ();
            }
            push @{$exp_hoa_all_regex1_vs_size{$sizeKey}}, $time;
          }elsif($regexCount <= 20){
            if(not exists $exp_hoa_all_regex2_vs_size{$sizeKey}){
              @{$exp_hoa_all_regex2_vs_size{$sizeKey}} = ();
            }
            push @{$exp_hoa_all_regex2_vs_size{$sizeKey}}, $time;
          }elsif($regexCount <= 30){
            if(not exists $exp_hoa_all_regex3_vs_size{$sizeKey}){
              @{$exp_hoa_all_regex3_vs_size{$sizeKey}} = ();
            }
            push @{$exp_hoa_all_regex3_vs_size{$sizeKey}}, $time;
          }elsif($regexCount <= 40){
            if(not exists $exp_hoa_all_regex4_vs_size{$sizeKey}){
              @{$exp_hoa_all_regex4_vs_size{$sizeKey}} = ();
            }
            push @{$exp_hoa_all_regex4_vs_size{$sizeKey}}, $time;
          }
        }
        $regexCount += 1;
        next;
      }
      if( $line =~ /^server\/ExpireA: ([0-9]+)$/){
        my $time = int($1/1000);
        if(not exists $exp_hoa_all_expireA_vs_size{$sizeKey}){
          @{$exp_hoa_all_expireA_vs_size{$sizeKey}} = ();
        }
        push @{$exp_hoa_all_expireA_vs_size{$sizeKey}}, $time;
        next;
      }
      if( $line =~ /^server\/DeleteA: ([0-9]+)$/){
        my $time = int($1/1000);
        if($path =~/noexpire/){
          if(not exists $noexp_hoa_all_deleteA_vs_size{$sizeKey}){
            @{$noexp_hoa_all_deleteA_vs_size{$sizeKey}} = ();
          }
          push @{$noexp_hoa_all_deleteA_vs_size{$sizeKey}}, $time;
        }else {
          if(not exists $exp_hoa_all_deleteA_vs_size{$sizeKey}){
            @{$exp_hoa_all_deleteA_vs_size{$sizeKey}} = ();
          }
          push @{$exp_hoa_all_deleteA_vs_size{$sizeKey}}, $time;
        }
        next;
      }

      if($line =~ /^server\/Range: ([0-9]+)$/){
        $rangeCount += 1;
        my $time = int($1/1000);
        # First Range is for "all" data
        if($rangeCount <= 1){
          if($path =~/noexpire/){
            $noexp_req_all{$sizeKey} = $time;
          }else {
            $exp_req_all{$sizeKey} = $time;
          }
        }
        # Remaining lines are various sizes
        # First 10 lines are 10% of total size
        elsif($rangeCount <= 11){
          if($path =~/noexpire/){
            if(not exists $noexp_req_10m{$sizeKey}){
              $noexp_req_10m{$sizeKey} = 0;
              $noexp_req_10c{$sizeKey} = 0;
            }
            $noexp_req_10m{$sizeKey} += $time;
            $noexp_req_10c{$sizeKey} += 1;
            if(not exists $noexp_hoa_all_range10_vs_size{$sizeKey}){
              @{$noexp_hoa_all_range10_vs_size{$sizeKey}} = ();
            }
            push @{$noexp_hoa_all_range10_vs_size{$sizeKey}}, $time;
          }else {
            if(not exists $exp_req_10m{$sizeKey}){
              $exp_req_10m{$sizeKey} = 0;
              $exp_req_10c{$sizeKey} = 0;
            }
            $exp_req_10m{$sizeKey} += $time;
            $exp_req_10c{$sizeKey} += 1;
            if(not exists $exp_hoa_all_range10_vs_size{$sizeKey}){
              @{$exp_hoa_all_range10_vs_size{$sizeKey}} = ();
            }
            push @{$exp_hoa_all_range10_vs_size{$sizeKey}}, $time;
          }
        }
        # Next 20 lines are 5% of total size
        elsif($rangeCount <= 31){
          if($path =~/noexpire/){
            if(not exists $noexp_req_5m{$sizeKey}){
              $noexp_req_5m{$sizeKey} = 0;
              $noexp_req_5c{$sizeKey} = 0;
            }
            $noexp_req_5m{$sizeKey} += $time;
            $noexp_req_5c{$sizeKey} += 1;
            if(not exists $noexp_hoa_all_range5_vs_size{$sizeKey}){
              @{$noexp_hoa_all_range5_vs_size{$sizeKey}} = ();
            }
            push @{$noexp_hoa_all_range5_vs_size{$sizeKey}}, $time;
          }else {
            if(not exists $exp_req_5m{$sizeKey}){
              $exp_req_5m{$sizeKey} = 0;
              $exp_req_5c{$sizeKey} = 0;
            }
            $exp_req_5m{$sizeKey} += $time;
            $exp_req_5c{$sizeKey} += 1;
            if(not exists $exp_hoa_all_range5_vs_size{$sizeKey}){
              @{$exp_hoa_all_range5_vs_size{$sizeKey}} = ();
            }
            push @{$exp_hoa_all_range5_vs_size{$sizeKey}}, $time;
          }
        }
        # Next 100 lines are 1% of total size
        elsif($rangeCount <= 131){
          if($path =~/noexpire/){
            if(not exists $noexp_req_1m{$sizeKey}){
              $noexp_req_1m{$sizeKey} = 0;
              $noexp_req_1c{$sizeKey} = 0;
            }
            $noexp_req_1m{$sizeKey} += $time;
            $noexp_req_1c{$sizeKey} += 1;
            if(not exists $noexp_hoa_all_range1_vs_size{$sizeKey}){
              @{$noexp_hoa_all_range1_vs_size{$sizeKey}} = ();
            }
            push @{$noexp_hoa_all_range1_vs_size{$sizeKey}}, $time;
          }else {
            if(not exists $exp_req_1m{$sizeKey}){
              $exp_req_1m{$sizeKey} = 0;
              $exp_req_1c{$sizeKey} = 0;
            }
            $exp_req_1m{$sizeKey} += $time;
            $exp_req_1c{$sizeKey} += 1;
            # Insert raw value for size-vs-timing boxes
            if(not exists $exp_hoa_all_range1_vs_size{$sizeKey}){
              @{$exp_hoa_all_range1_vs_size{$sizeKey}} = ();
            }
            push @{$exp_hoa_all_range1_vs_size{$sizeKey}}, $time;
          }
        }
        next;
      } # End range
      # Snapshot queries, from 2.5 -> 97.5 increment 5
      if($line =~ /^server\/Snapshot: ([0-9]+)$/){
        my $time = int($1/1000);
        if($path =~/noexpire/){
          if(not exists $noexp_hoa_snap_vs_size{$sizeKey}){
            @{$noexp_hoa_snap_vs_size{$sizeKey}} = ();
          }
          push @{$noexp_hoa_snap_vs_size{$sizeKey}}, $time;
        }else {
          if(not exists $exp_hoa_snap_vs_size{$sizeKey}){
            @{$exp_hoa_snap_vs_size{$sizeKey}} = ();
          }
          push @{$exp_hoa_snap_vs_size{$sizeKey}}, $time;
        }
      }
    }
  }
  close $file;

}

print "\n";

open (OUT95FILE, ">worldmodel-95pct-all.csv");

# Now save the size-vs-95pct CSV
print "worldmodel-insert-size.csv\n";
open (OUTFILE, '>worldmodel-insert-size.csv');
print OUTFILE "World Model Size,Insert Time with Expiration,Insert Time without Expiration\n";
foreach my $sz (sort {$a <=> $b} (keys(%exp_size_vs_insert))) {
  my $exp = $exp_size_vs_insert{$sz};
  my $noexp = (exists $noexp_size_vs_insert{$sz}) ? $noexp_size_vs_insert{$sz} : 0;
  print OUTFILE "$sz,$exp,$noexp\n";
}
close (OUTFILE);

# Now the all range query 
print "worldmodel-range-time.csv\n";
open (OUTFILE, '>worldmodel-range-time.csv');
print OUTFILE "World Model Size,Range All with Expiration,Range All without Expiration\n";
foreach my $sz (sort {$a <=> $b} (keys(%exp_req_all))) {
  my $exp = $exp_req_all{$sz};
  my $noexp = (exists $noexp_req_all{$sz}) ? $noexp_req_all{$sz} : 0;
  print OUTFILE "$sz,$exp,$noexp\n";
}
close (OUTFILE);

# Now the 1% boxes
{
  print "worldmodel-range01-boxes.csv\n";
  open (OUTFILE, '>worldmodel-range01-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_range1_vs_size);
  my $noexp_box = meanAndStdDev(\%noexp_hoa_all_range1_vs_size);
  print OUTFILE "Size,Mean (E),StdErr (E),-StdDev (E),+StdDev (E),99Pct (E),Mean (N),StdErr (N),-StdDev (N),+StdDev (N),99Pct (N)\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))) {
    my @earr = @{$exp_box->{$sz}};
    if(exists $noexp_box->{$sz}){
      my @narr = @{$noexp_box->{$sz}};
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],$narr[0],$narr[1],$narr[2],$narr[3],$narr[4]\n";
      print OUT95FILE "range01,$sz,$earr[5],$narr[5]\n";
    }else {
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],,,,,\n";
      print OUT95FILE "range01,$sz,$earr[5],\n";
    }
  }
  close OUTFILE
}

# Now the 5% boxes
{
  print "worldmodel-range05-boxes.csv\n";
  open (OUTFILE, '>worldmodel-range05-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_range5_vs_size);
  my $noexp_box = meanAndStdDev(\%noexp_hoa_all_range5_vs_size);
  print OUTFILE "Size,Mean (E),StdErr (E),-StdDev (E),+StdDev (E),99Pct (E),Mean (N),StdErr (N),-StdDev (N),+StdDev (N),99Pct (N)\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))) {
    my @earr = @{$exp_box->{$sz}};
    if(exists $noexp_box->{$sz}){
      my @narr = @{$noexp_box->{$sz}};
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],$narr[0],$narr[1],$narr[2],$narr[3],$narr[4]\n";
      print OUT95FILE "range05,$sz,$earr[5],$narr[5]\n";
    }else {
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],,,,,\n";
      print OUT95FILE "range05,$sz,$earr[5],\n";
    }
  }
  close OUTFILE
}

# Now the 10% boxes
{
  print "worldmodel-range10-boxes.csv\n";
  open (OUTFILE, '>worldmodel-range10-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_range10_vs_size);
  my $noexp_box = meanAndStdDev(\%noexp_hoa_all_range10_vs_size);
  print OUTFILE "Size,Mean (E),StdErr (E),-StdDev (E),+StdDev (E),99Pct (E),Mean (N),StdErr (N),-StdDev (N),+StdDev (N),99Pct (N)\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))) {
    my @earr = @{$exp_box->{$sz}};
    if(exists $noexp_box->{$sz}){
      my @narr = @{$noexp_box->{$sz}};
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],$narr[0],$narr[1],$narr[2],$narr[3],$narr[4]\n";
      print OUT95FILE "range10,$sz,$earr[5],$narr[5]\n";
    }else {
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],,,,,\n";
      print OUT95FILE "range10,$sz,$earr[5],\n";
    }
  }
  close OUTFILE
}

# Now the Expiration boxes
{
  print "worldmodel-expire-boxes.csv\n";
  open (OUTFILE, '>worldmodel-expire-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_expireA_vs_size);
  print OUTFILE "Size,Mean,StdErr,-StdDev,+StdDev,99Pct\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))){
    my @earr = @{$exp_box->{$sz}};
    print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4]\n";
    print OUT95FILE "expire,$sz,$earr[5]\n";
  }
  close OUTFILE
}

# Now the Delete boxes
{
  print "worldmodel-delete-boxes.csv\n";
  open (OUTFILE, '>worldmodel-delete-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_deleteA_vs_size);
  my $noexp_box = meanAndStdDev(\%noexp_hoa_all_deleteA_vs_size);
  print OUTFILE "Size,Mean (E),StdErr (E),-StdDev (E),+StdDev (E),99Pct (E),Mean (N),StdErr (N),-StdDev (N),+StdDev (N),99Pct (N)\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))){
    my @earr = @{$exp_box->{$sz}};
    if(exists $noexp_box->{$sz}){
      my @narr = @{$noexp_box->{$sz}};
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],$narr[0],$narr[1],$narr[2],$narr[3],$narr[4]\n";
      print OUT95FILE "delete,$sz,$earr[5],$narr[5]\n";
    }else {
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[4],$earr[4],,,,,\n";
      print OUT95FILE "delete,$sz,$earr[5],\n";
    }
  }
  close OUTFILE
}

# Now the Regex 1 boxes
{
  print "worldmodel-regex1-boxes.csv\n";
  open (OUTFILE, '>worldmodel-regex1-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_regex1_vs_size);
  my $noexp_box = meanAndStdDev(\%noexp_hoa_all_regex1_vs_size);
  print OUTFILE "Size,Mean (E),StdErr (E),-StdDev (E),+StdDev (E),99Pct (E),Mean (N),StdErr (N),-StdDev (N),+StdDev (N),99Pct (E)\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))){
    my @earr = @{$exp_box->{$sz}};
    if(exists $noexp_box->{$sz}){
      my @narr = @{$noexp_box->{$sz}};
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],$narr[0],$narr[1],$narr[2],$narr[3],$narr[4]\n";
      print OUT95FILE "regex1,$sz,$earr[5],$narr[5]\n";
    }else {
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],,,,,\n";
      print OUT95FILE "regex1,$sz,$earr[5],\n";
    }
  }
  close OUTFILE
}

# Now the Regex 2 boxes
{
  print "worldmodel-regex2-boxes.csv\n";
  open (OUTFILE, '>worldmodel-regex2-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_regex2_vs_size);
  my $noexp_box = meanAndStdDev(\%noexp_hoa_all_regex2_vs_size);
  print OUTFILE "Size,Mean (E),StdErr (E),-StdDev (E),+StdDev (E),99Pct (E),Mean (N),StdErr (N),-StdDev (N),+StdDev (N),99Pct (N)\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))){
    my @earr = @{$exp_box->{$sz}};
    if(exists $noexp_box->{$sz}){
      my @narr = @{$noexp_box->{$sz}};
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],$narr[0],$narr[1],$narr[2],$narr[3],$narr[4]\n";
      print OUT95FILE "regex2,$sz,$earr[5],$narr[5]\n";
    }else {
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],,,,,\n";
      print OUT95FILE "regex2,$sz,$earr[5],\n";
    }
  }
  close OUTFILE
}

# Now the Regex 3 boxes
{
  print "worldmodel-regex3-boxes.csv\n";
  open (OUTFILE, '>worldmodel-regex3-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_regex3_vs_size);
  my $noexp_box = meanAndStdDev(\%noexp_hoa_all_regex3_vs_size);
  print OUTFILE "Size,Mean (E),StdErr (E),-StdDev (E),+StdDev (E),99Pct (E),Mean (N),StdErr (N),-StdDev (N),+StdDev (N),99Pct (N)\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))){
    my @earr = @{$exp_box->{$sz}};
    if(exists $noexp_box->{$sz}){
      my @narr = @{$noexp_box->{$sz}};
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],$narr[0],$narr[1],$narr[2],$narr[3],$narr[4]\n";
      print OUT95FILE "regex3,$sz,$earr[5],$narr[5]\n";
    }else {
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],,,,,\n";
      print OUT95FILE "regex3,$sz,$earr[5],\n";
    }
  }
  close OUTFILE
}

# Now the Regex 4 boxes
{
  print "worldmodel-regex4-boxes.csv\n";
  open (OUTFILE, '>worldmodel-regex4-boxes.csv');
  my $exp_box = meanAndStdDev(\%exp_hoa_all_regex4_vs_size);
  my $noexp_box = meanAndStdDev(\%noexp_hoa_all_regex4_vs_size);
  print OUTFILE "Size,Mean (E),StdErr (E),-StdDev (E),+StdDev (E),99Pct (E),Mean (N),StdErr (N),-StdDev (N),+StdDev (N),99Pct (N)\n";
  foreach my $sz (sort {$a <=> $b} (keys(%{$exp_box}))){
    my @earr = @{$exp_box->{$sz}};
    if(exists $noexp_box->{$sz}){
      my @narr = @{$noexp_box->{$sz}};
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],$narr[0],$narr[1],$narr[2],$narr[3],$narr[4]\n";
      print OUT95FILE "regex4,$sz,$earr[5],$narr[5]\n";
    }else {
      print OUTFILE "$sz,$earr[0],$earr[1],$earr[2],$earr[3],$earr[4],,,,,\n";
      print OUT95FILE "regex4,$sz,$earr[5],\n";
    }
  }
  close OUTFILE
}

# Now the Snapshot values
# With expiration
{ 
  print "worldmodel-snapshot-exp-values.csv\n";
  open (OUTFILE, '>worldmodel-snapshot-exp-values.csv');
  my @valArr = ();
  print OUTFILE "Percentage,";
  foreach my $sz (sort {$a <=> $b} (keys(%exp_hoa_snap_vs_size))){
    print OUTFILE "$sz,";
    my @arr = @{$exp_hoa_snap_vs_size{$sz}};
    for (my $i = 0; $i < 20; $i += 1){
      if(not exists $valArr[$i]){
        @{$valArr[$i]} = ();
      }
      push @{$valArr[$i]},$arr[$i];
    }
  }
  print OUTFILE "\n";
  # Now print the array that was built
  for (my $i = 0; $i < 20; $i += 1){
    print OUTFILE ($i*5+2.5).",";
    foreach my $val (@{$valArr[$i]}) {
      print OUTFILE "$val,";
    }
    print OUTFILE "\n";
  }
  close OUTFILE
}
# Without expiration
{ 
  print "worldmodel-snapshot-noexp-values.csv\n";
  open (OUTFILE, '>worldmodel-snapshot-noexp-values.csv');
  my @valArr = ();
  print OUTFILE "Percentage,";
  foreach my $sz (sort {$a <=> $b} (keys(%noexp_hoa_snap_vs_size))){
    print OUTFILE "$sz,";
    my @arr = @{$noexp_hoa_snap_vs_size{$sz}};
    for (my $i = 0; $i < 20; $i += 1){
      if(not exists $valArr[$i]){
        @{$valArr[$i]} = ();
      }
      push @{$valArr[$i]},$arr[$i];
    }
  }
  print OUTFILE "\n";
  # Now print the array that was built
  for (my $i = 0; $i < 20; $i += 1){
    print OUTFILE ($i*5+2.5).",";
    foreach my $val (@{$valArr[$i]}) {
      print OUTFILE "$val,";
    }
    print OUTFILE "\n";
  }
  close OUTFILE
}

close OUT95FILE;
