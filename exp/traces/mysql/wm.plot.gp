#!/usr/bin/gnuplot
#set bmargin 2.5;

set xlabel 'World Model Size (No. of Attributes)' 
set ylabel 'Time ({/Symbol m}s)'
set key outside right top box width -1 height 2 enhanced spacing 1
set terminal postscript enhanced size 10.5in,5in color #font "Helvetica" 8; 
set datafile sep ','; 
set log xy;
set title "{/=15 MySQL World Model Performance: 1% Range Request}"
set output "mysql.wm.range01.eps";
set key inside bottom right box width -1 height 2 enhanced spacing 1
set yrange [600:22000]; 
set xrange [900:110000];
plot 'worldmodel-range01-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-range01-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-range01-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-range01-boxes.csv' u 1:11 title '99 Percentile'  w linespoints lc 2


set title "{/=15 MySQL World Model Performance: 5% Range Request}"
set output "mysql.wm.range05.eps";
set key inside bottom right box width -1 height 2 enhanced spacing 1
set yrange [900:120000]; 
plot 'worldmodel-range05-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-range05-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-range05-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-range05-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2

set title "{/=15 MySQL World Model Performance: 10% Range Request}"
set output "mysql.wm.range10.eps";
set key inside bottom right box width -1 height 2 enhanced spacing 1
set yrange [1000:350000]; 
plot 'worldmodel-range10-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-range10-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-range10-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-range10-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2

set title "{/=15 MySQL World Model Performance: Expiring Attributes}"
set output "mysql.wm.expire.eps";
set key at 1e5,7e3 box width -1 height 2 enhanced spacing 1
set yrange [2000:10000];
unset log y;
plot 'worldmodel-expire-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-expire-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1

set title "{/=15 MySQL World Model Performance: Deleting Attributes}"
set output "mysql.wm.delete.eps";
set key at 1e5,6.5e3 box width -1 height 2 enhanced spacing 1
set yrange [2000:10000];
unset log y;
plot 'worldmodel-delete-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-delete-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-delete-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-delete-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2

set title "{/=15 MySQL World Model Performance: Regex Search 1}"
set output "mysql.wm.regex1.eps";
set key inside bottom right box width -1 height 2 enhanced spacing 1
set yrange [300:60000];
set log y
plot 'worldmodel-regex1-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-regex1-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-regex1-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-regex1-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2

set title "{/=15 MySQL World Model Performance: Regex Search 2}"
set output "mysql.wm.regex2.eps";
set yrange [20:210000];
plot 'worldmodel-regex2-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-regex2-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-regex2-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-regex2-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2

set title "{/=15 MySQL World Model Performance: Regex Search 3}"
set output "mysql.wm.regex3.eps";
set yrange [1000:320000];
plot 'worldmodel-regex3-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-regex3-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-regex3-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-regex3-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2

set title "{/=15 MySQL World Model Performance: Regex Search 4}"
set output "mysql.wm.regex4.eps";
set yrange [1000:320000];
plot 'worldmodel-regex4-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-regex4-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-regex4-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-regex4-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2

set title "{/=15 MySQL World Model Performance: 5% Snapshot (1-Minute Expirations)}"
set key outside right top box width 0 height 2 enhanced spacing 1
set xlabel 'Snapshot Position in Data Set (Percentage)'
set ylabel 'Query Time ({/Symbol m}s)'
set output "mysql.wm.snapshot.exp.eps"
set yrange [600:*];
set xrange [0:100];
unset log x;
set log y;
plot 'worldmodel-snapshot-exp-values.csv' u 1:2 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:3 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:4 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:5 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:6 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:7 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:8 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:9 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:10 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:11 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:12 title columnhead w linespoints

set title "{/=15 MySQL World Model Performance: 5% Snapshot (No Expirations)}"
set key outside right top box width 0 height 2 enhanced spacing 1
set xlabel 'Snapshot Position in Data Set (Percentage)'
set ylabel 'Query Time ({/Symbol m}s)'
set output "mysql.wm.snapshot.noexp.eps"
set yrange [700:*];
set xrange [0:100];
unset log x;
set log y;
plot 'worldmodel-snapshot-noexp-values.csv' u 1:2 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:3 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:4 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:5 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:6 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:7 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:8 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:9 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:10 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:11 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:12 title columnhead w linespoints

set title "{/=15 MySQL World Model Performance: Serial Insertion}"
set key inside right bottom box width 0 height 2 enhanced spacing 1
set xlabel 'World Model Size (No. of Attributes)' 
set ylabel 'Insert Time ({/Symbol m}s)'
set output "mysql.wm.insert.95.eps"
set yrange [15500:18000];
set xrange [900:2.5e5];
set log x
unset log y
plot 'worldmodel-insert-95.csv' u 1:2 title 'With Receiver' w linespoints,\
     'worldmodel-insert-95.csv' u 1:3 title 'Without Receiver' w linespoints


set title "{/=15 MySQL World Model Performance: Serial Insertion}"
set output "mysql.wm.insert.cdf.20k-200k.eps";
set key inside right bottom box width 0 height 2 enhanced spacing 1
set xlabel 'Insert Time ({/Symbol m}s)' 
set ylabel 'CDF';
set yrange [0:1.05];
set xrange [9000:1e5];
unset log y
#set log x;
plot 'i20k-cdf.csv' u 1:2 title '20K Attributes With Receiver' w linespoints lc 1 lt 1,\
     'i20k_nr-cdf.csv' u 1:2 title '20K Attributes No Receiver' w linespoints lc 1 lt 2,\
     'i200k-cdf.csv' u 1:2 title '200K Attributes With Receiver' w linespoints lc 2 lt 1,\
     'i200k_nr-cdf.csv' u 1:2 title '200K Attributes No Receiver' w linespoints lc 2 lt 3
