#!/usr/bin/gnuplot
#set bmargin 2.5;

set title "{/=15 Aggregator Performance:}\n5 Replay Sensors and \"Select-5\" Solvers"
set xlabel 'Time (seconds)' 
set xtics rotate
set ylabel 'Samples/sec'
set key outside right top box width -2 height 2 enhanced spacing 1
set terminal postscript enhanced size 10.5in,5in color #font "Helvetica" 8; 
set output "agg4.rate.eps";
set timefmt '%Y-%m-%d %H:%M:%S'; 
set datafile sep ','; 
set yrange [*:*]; 
set xrange [*:*];
plot 'agg4-1-10.csv' u 1:2 title '1 Solvers, 1X Speed' w lines lc 1 lt 1,\
     'agg4-1-20.csv' u 1:2 title '1 Solvers, 2X Speed' w lines lc 1 lt 2,\
     'agg4-1-30.csv' u 1:2 title '1 Solvers, 3X Speed' w lines lc 1 lt 3,\
     'agg4-1-40.csv' u 1:2 title '1 Solvers, 4X Speed' w lines lc 1 lt 4,\
     'agg4-3-10.csv' u 1:2 title '3 Solvers, 1X Speed' w lines lc 2 lt 1,\
     'agg4-3-20.csv' u 1:2 title '3 Solvers, 2X Speed' w lines lc 2 lt 2,\
     'agg4-3-25.csv' u 1:2 title '3 Solvers, 2.5X Speed' w lines lc 2 lt 3,\
     'agg4-3-30.csv' u 1:2 title '3 Solvers, 3X Speed' w lines lc 2 lt 4,\
     'agg4-6-10.csv' u 1:2 title '6 Solvers, 1X Speed' w lines lc 3 lt 1,\
     'agg4-6-15.csv' u 1:2 title '6 Solvers, 1.5X Speed' w lines lc 3 lt 2,\
     'agg4-6-20-timeout.csv' u 1:2 title '6 Solvers, 2X Speed' w lines lc 3 lt 3,\
     'agg4-9-10.csv' u 1:2 title '9 Solvers, 1X Speed' w lines lc 4 lt 1,\
     'agg4-9-125.csv' u 1:2 title '9 Solvers, 1.25X Speed' w lines lc 4 lt 2,\
     'agg4-9-15-timeout.csv' u 1:2 title '9 Solvers, 1.5X Speed' w lines lc 4 lt 3


set output "agg4.proc.eps";
set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set key outside top right  box width -3 height 1 enhanced spacing 1
set log y
set yrange [5:1.2e6]
plot 'agg4-1-10.csv' u 1:3 title '1 Solvers, 1X Speed' w lines lc 1 lt 1,\
     'agg4-1-20.csv' u 1:3 title '1 Solvers, 2X Speed' w lines lc 1 lt 2,\
     'agg4-1-30.csv' u 1:3 title '1 Solvers, 3X Speed' w lines lc 1 lt 3,\
     'agg4-1-40.csv' u 1:3 title '1 Solvers, 4X Speed' w lines lc 1 lt 4,\
     'agg4-3-10.csv' u 1:3 title '3 Solvers, 1X Speed' w lines lc 2 lt 1,\
     'agg4-3-20.csv' u 1:3 title '3 Solvers, 2X Speed' w lines lc 2 lt 2,\
     'agg4-3-25.csv' u 1:3 title '3 Solvers, 2.5X Speed' w lines lc 2 lt 3,\
     'agg4-3-30.csv' u 1:3 title '3 Solvers, 3X Speed' w lines lc 2 lt 4,\
     'agg4-6-10.csv' u 1:3 title '6 Solvers, 1X Speed' w lines lc 3 lt 1,\
     'agg4-6-15.csv' u 1:3 title '6 Solvers, 1.5X Speed' w lines lc 3 lt 2,\
     'agg4-6-20-timeout.csv' u 1:3 title '6 Solvers, 2X Speed' w lines lc 3 lt 3,\
     'agg4-9-10.csv' u 1:3 title '9 Solvers, 1X Speed' w lines lc 4 lt 1,\
     'agg4-9-125.csv' u 1:3 title '9 Solvers, 1.25X Speed' w lines lc 4 lt 2,\
     'agg4-9-15-timeout.csv' u 1:3 title '9 Solvers, 1.5X Speed' w lines lc 4 lt 3
unset log y

set output "agg4.life.eps";
set key outside top right  box width -2 height 2 enhanced spacing 1
set ylabel 'Sample Lifetime ({/Symbol m}s)' 
set log y
set yrange [100:5e7]
plot 'agg4-1-10.csv' u 1:4 title '1 Solvers, 1X Speed' w lines lc 1 lt 1,\
     'agg4-1-20.csv' u 1:4 title '1 Solvers, 2X Speed' w lines lc 1 lt 2,\
     'agg4-1-30.csv' u 1:4 title '1 Solvers, 3X Speed' w lines lc 1 lt 3,\
     'agg4-1-40.csv' u 1:4 title '1 Solvers, 4X Speed' w lines lc 1 lt 4,\
     'agg4-3-10.csv' u 1:4 title '3 Solvers, 1X Speed' w lines lc 2 lt 1,\
     'agg4-3-20.csv' u 1:4 title '3 Solvers, 2X Speed' w lines lc 2 lt 2,\
     'agg4-3-25.csv' u 1:4 title '3 Solvers, 2.5X Speed' w lines lc 2 lt 3,\
     'agg4-3-30.csv' u 1:4 title '3 Solvers, 3X Speed' w lines lc 2 lt 4,\
     'agg4-6-10.csv' u 1:4 title '6 Solvers, 1X Speed' w lines lc 3 lt 1,\
     'agg4-6-15.csv' u 1:4 title '6 Solvers, 1.5X Speed' w lines lc 3 lt 2,\
     'agg4-6-20-timeout.csv' u 1:4 title '6 Solvers, 2X Speed' w lines lc 3 lt 3,\
     'agg4-9-10.csv' u 1:4 title '9 Solvers, 1X Speed' w lines lc 4 lt 1,\
     'agg4-9-125.csv' u 1:4 title '9 Solvers, 1.25X Speed' w lines lc 4 lt 2,\
     'agg4-9-15-timeout.csv' u 1:4 title '9 Solvers, 1.5X Speed' w lines lc 4 lt 3
unset log y

set yrange[*:*]

set output "agg4.1.svr.eps"
set title "{/=15 Aggregator Performance: 5 Replay Sensors and 1 \"Select-5\" Solver}"
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set ylabel 'Service Rate (Samples per Second)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-1solver.csv' u 1:2 title columnhead w lp,\
     'agg4-percentiles-1solver.csv' u 1:3 title columnhead w lp


set output "agg4.1.svp.eps"
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Processing Time ({/Symbol m}s)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-1solver.csv' u 1:4 title columnhead w lp,\
     'agg4-percentiles-1solver.csv' u 1:5 title columnhead w lp


set output "agg4.1.svl.eps"
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Lifetime ({/Symbol m}s)' 
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-1solver.csv' u 1:6 title columnhead w lp,\
     'agg4-percentiles-1solver.csv' u 1:7 title columnhead w lp

set output "agg4.3.svr.eps"
set title "{/=15 Aggregator Performance: 5 Replay Sensors and 3 \"Select-5\" Solver}"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Service Rate (Samples per Second)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-3solver.csv' u 1:2 title columnhead w lp,\
     'agg4-percentiles-3solver.csv' u 1:3 title columnhead w lp


set output "agg4.3.svp.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Processing Time ({/Symbol m}s)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-3solver.csv' u 1:4 title columnhead w lp,\
     'agg4-percentiles-3solver.csv' u 1:5 title columnhead w lp


set output "agg4.3.svl.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Lifetime ({/Symbol m}s)' 
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-3solver.csv' u 1:6 title columnhead w lp,\
     'agg4-percentiles-3solver.csv' u 1:7 title columnhead w lp

set output "agg4.6.svr.eps"
set title "{/=15 Aggregator Performance: 5 Replay Sensors and 6 \"Select-5\" Solver}"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Service Rate (Samples per Second)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-6solver.csv' u 1:2 title columnhead w lp,\
     'agg4-percentiles-6solver.csv' u 1:3 title columnhead w lp


set output "agg4.6.svp.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Processing Time ({/Symbol m}s)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-6solver.csv' u 1:4 title columnhead w lp,\
     'agg4-percentiles-6solver.csv' u 1:5 title columnhead w lp


set output "agg4.6.svl.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Lifetime ({/Symbol m}s)' 
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-6solver.csv' u 1:6 title columnhead w lp,\
     'agg4-percentiles-6solver.csv' u 1:7 title columnhead w lp

set output "agg4.9.svr.eps"
set title "{/=15 Aggregator Performance: 5 Replay Sensors and 9 \"Select-5\" Solver}"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Service Rate (Samples per Second)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-9solver.csv' u 1:2 title columnhead w lp,\
     'agg4-percentiles-9solver.csv' u 1:3 title columnhead w lp


set output "agg4.9.svp.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Processing Time ({/Symbol m}s)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-9solver.csv' u 1:4 title columnhead w lp,\
     'agg4-percentiles-9solver.csv' u 1:5 title columnhead w lp


set output "agg4.9.svl.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Lifetime ({/Symbol m}s)' 
set xlabel 'Sensor Speed (Multiple of Recorded)'
plot 'agg4-percentiles-9solver.csv' u 1:6 title columnhead w lp,\
     'agg4-percentiles-9solver.csv' u 1:7 title columnhead w lp


set output "agg4.rate_vs.proc.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set title "{/=15 Impact of Service Rate on Processing Time}"
set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set xlabel 'Average Sample Arrival Rate ({/Symbol m}s)'
set xrange [80:0]
set yrange [.1:1000]
set log y
plot 'agg4-1.rate_vs.csv' u 1:2:3 title '1 Solver' w yerrorbars,\
     'agg4-3.rate_vs.csv' u 1:2:3 title '3 Solvers' w yerrorbars,\
     'agg4-6.rate_vs.csv' u 1:2:3 title '6 Solvers' w yerrorbars,\
     'agg4-9.rate_vs.csv' u 1:2:3 title '9 Solvers' w yerrorbars
unset log y


set output "agg4.rate_vs.life.eps"
set title "{/=15 Impact of Service Rate on Sample Lifetime}"
set ylabel 'Sample Lifetime ({/Symbol m}s)'
set log y
set yrange [80:1e6]
plot 'agg4-1.rate_vs.csv' u 1:4:5 title '1 Solver' w yerrorbars,\
     'agg4-3.rate_vs.csv' u 1:4:5 title '3 Solvers' w yerrorbars,\
     'agg4-6.rate_vs.csv' u 1:4:5 title '6 Solvers' w yerrorbars,\
     'agg4-9.rate_vs.csv' u 1:4:5 title '9 Solvers' w yerrorbars
unset log y
