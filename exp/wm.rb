# Experimental properties
# Override at command line like this:
# omf exec scale.rb -- --numSensors 5 --replaySpeed 2.5

defProperty('useSQL','false','')
defProperty('numAttributes',1000,'')
defProperty('expire','false','')
defProperty('mainWait',300,'')
defProperty('individual','false','')
defProperty('receiver','true','')
defProperty('insertDelay',250,'')
defProperty('expireDelay',200,'')
defProperty('deleteDelay',3000,'')
defProperty('numSizes',5,'')

# Constants, file names, and paths
defProperty('sqliteWmBinary','/usr/local/bin/owl/sqlite3_world_model_server','')
defProperty('mysqlWmBinary','/usr/local/bin/owl/mysql_world_model_server','')
defProperty('wmLogFile', '/worldmodel.log', '')
defProperty('sqlUser', 'owl','')
defProperty('sqlPass','password','')
defProperty('sqlConfig','/mysqlwm.conf','')
defProperty('profileJarFile','/wm-profiler.jar','')
defProperty('profileLogFile', '/profiler.log', '')
defProperty('profileRcvClass','com.owlplatform.wmprofile.Receiver','')
defProperty('receiverLogFile', '/receiver.log', '')


#allTopology = Topology['system:topo:imaged']
allTopology = defTopology('nodes','node1-1.grid.orbit-lab.org,node1-2.grid.orbit-lab.org,node10-11.grid.orbit-lab.org,node10-14.grid.orbit-lab.org,node10-17.grid.orbit-lab.org,node10-7.grid.orbit-lab.org,node11-1.grid.orbit-lab.org,node11-17.grid.orbit-lab.org,node11-7.grid.orbit-lab.org,node13-13.grid.orbit-lab.org,node13-8.grid.orbit-lab.org,node14-10.grid.orbit-lab.org,node14-7.grid.orbit-lab.org,node5-5.grid.orbit-lab.org,node6-15.grid.orbit-lab.org,node6-6.grid.orbit-lab.org,node7-10.grid.orbit-lab.org,node7-14.grid.orbit-lab.org,node7-7.grid.orbit-lab.org,node8-13.grid.orbit-lab.org,node8-18.grid.orbit-lab.org,node8-3.grid.orbit-lab.org,node8-8.grid.orbit-lab.org')
potWmTopology = defTopology('possibleworldmodels','node1-1.grid.orbit-lab.org,node1-2.grid.orbit-lab.org,node10-11.grid.orbit-lab.org,node10-14.grid.orbit-lab.org,node10-17.grid.orbit-lab.org,node10-7.grid.orbit-lab.org')
profTopology = defTopology('profilers','node11-1.grid.orbit-lab.org,node11-17.grid.orbit-lab.org,node11-7.grid.orbit-lab.org,node13-13.grid.orbit-lab.org,node13-8.grid.orbit-lab.org,node14-10.grid.orbit-lab.org')
profRecvTopology = defTopology('profiler-receivers','node14-7.grid.orbit-lab.org,node5-5.grid.orbit-lab.org,node6-15.grid.orbit-lab.org,node6-6.grid.orbit-lab.org,node7-10.grid.orbit-lab.org,node7-14.grid.orbit-lab.org')

wmTopString = "";
i = 0
potWmTopology.nodes.each{|nodeName|
	wmTopString += nodeName.to_s
	i += 1
	break if i >= property.numSizes.to_s.to_i
	wmTopString += ","
}

wmTopology = defTopology('worldmodels',wmTopString)

receiverGroups = Hash.new


def main(topology, rcvGroups, wmTopology, profTopology, profRecvTopology) 
	wmHash = Hash.new
	sizeHash = Hash.new
	nodeIpMap = 
	{
		# World Model
		'node1-1.grid.orbit-lab.org' => '192.168.1.1',
		'node1-2.grid.orbit-lab.org' => '192.168.1.2',
		'node10-11.grid.orbit-lab.org' => '192.168.1.3',
		'node10-14.grid.orbit-lab.org' => '192.168.1.4',
		'node10-17.grid.orbit-lab.org' => '192.168.1.5',
		'node10-7.grid.orbit-lab.org' => '192.168.1.6',
		'node11-1.grid.orbit-lab.org' => '192.168.1.7',
		'node11-17.grid.orbit-lab.org' => '192.168.1.8',
		'node11-7.grid.orbit-lab.org' => '192.168.1.9',
		'node13-13.grid.orbit-lab.org' => '192.168.1.10',
		'node13-8.grid.orbit-lab.org' => '192.168.1.11',
		'node14-10.grid.orbit-lab.org' => '192.168.1.12',
		'node14-7.grid.orbit-lab.org' => '192.168.1.13',
		'node5-5.grid.orbit-lab.org' => '192.168.1.14',
		'node6-15.grid.orbit-lab.org' => '192.168.1.15',
		'node6-6.grid.orbit-lab.org' => '192.168.1.16',
		'node7-10.grid.orbit-lab.org' => '192.168.1.17',
		'node7-14.grid.orbit-lab.org' => '192.168.1.18',
		'node7-7.grid.orbit-lab.org' => '192.168.1.19',
		'node8-13.grid.orbit-lab.org' => '192.168.1.20',
		'node8-18.grid.orbit-lab.org' => '192.168.1.21',
		'node8-3.grid.orbit-lab.org' => '192.168.1.22',
		'node8-8.grid.orbit-lab.org' => '192.168.1.23'
	}

	# For each non-WM node, assign a WM node
	# Ensure one WM node for each prof/receiver node

	numWMs = wmTopology.size
	currSizeMult = 1

	wmTopology.nodes.each{ |nodeName|
		sizeHash[nodeName.to_s] = currSizeMult * property.numAttributes.to_s.to_i;
		currSizeMult += 1
	}

	currWMCount = 0
	currSizeMult = 1

	profTopology.nodes.each{ |nodeName|
		wmHash[nodeName.to_s] = nodeIpMap[wmTopology.nodes[currWMCount].to_s];
		sizeHash[nodeName.to_s] = currSizeMult * property.numAttributes.to_s.to_i;
		currWMCount += 1;
		currSizeMult += 1;
		break if currWMCount >= numWMs;
	}

	currWMCount = 0
	currSizeMult = 1

	profRecvTopology.nodes.each{ |nodeName|
		wmHash[nodeName.to_s] = nodeIpMap[wmTopology.nodes[currWMCount].to_s];
		sizeHash[nodeName.to_s] = currSizeMult * property.numAttributes.to_s.to_i;
		currWMCount += 1;
		currSizeMult += 1;
		break if (currWMCount >= numWMs);
	}

	sizeHash.each{ |entry|
		info "#{entry}"
	}

	info "##### SETTINGS #####"
	info "Use SQL? #{property.useSQL}"
	info "No. Attrs: #{property.numAttributes}"
	info "Expire? #{property.expire}"
	info "Individual? #{property.individual}"
	info "Receiver? #{property.receiver}"
	info "####################"


	topology.nodes.each { |nodeName|
		rcvGroups[nodeName.to_s].net.e0.ip = nodeIpMap[nodeName.to_s]
		info "Set #{nodeName} IP address #{nodeIpMap[nodeName.to_s]}"
	}

	info "Waiting 5 seconds for networking to resolve"
	wait 5 
	info "Starting world model"

	idx = 0


	info "Clearing world model files"
	wmTopology.nodes.each { |nodeName|
		rcvGroups[nodeName.to_s].exec("rm /world_*")
	}

	if property.useSQL.to_s == "true"	
		wmTopology.nodes.each{ |nodeName|
			rcvGroups[nodeName.to_s].exec("mysql -u owl -ppassword -D world_model_db -e 'delete from AttributeValues where 1=1; delete from Uris where 1=1; delete from Origins where 1=1; delete from Attributes where 1=1;'");
			info "Deleting old data from MySQL DB";
		}

		wait 5

		wmTopology.nodes.each { |nodeName|
			rcvGroups[nodeName.to_s].exec("unbuffer #{property.mysqlWmBinary} #{property.sqlConfig} >#{property.wmLogFile} 2>&1")
			info "Started MySQL world model on #{nodeName}"
		}
	else
		wmTopology.nodes.each { |nodeName|
			rcvGroups[nodeName.to_s].exec("unbuffer #{property.sqliteWmBinary} >#{property.wmLogFile} 2>&1")
			info "Started SQLite world model on #{nodeName}"
		}
	end
	
	info "Waiting 10 seconds for world model to start/stabilize"
	wait 10

	if property.receiver.to_s == "true"
		info "Starting receiver"

		currWmCount = 0

		profRecvTopology.nodes.each{ |nodeName|
			wmIp = wmHash[nodeName.to_s]
			rcvGroups[nodeName.to_s].exec("java -cp #{property.profileJarFile} #{property.profileRcvClass} #{wmIp} 7010 >#{property.receiverLogFile} 2>&1")
			info "java -cp #{property.profileJarFile} #{property.profileRcvClass} #{wmIp} 7010 >#{property.receiverLogFile} 2>&1"
			currWmCount += 1
			break if currWmCount >= numWMs
		}

		info "Waiting 5 seconds between receiver/profiler"
		wait 5
	end
	info "Starting profiler"

	profOpts = property.expire.to_s == "true" ? "--expire" : ""
	if (property.individual.to_s == "true")
		profOpts += " --individual"
	end
	profOpts += " --insertDelay #{property.insertDelay} --expireDelay #{property.expireDelay} --deleteDelay #{property.deleteDelay}"
	
	currWmCount = 0

	profTopology.nodes.each{ |nodeName|
		wmIp = wmHash[nodeName.to_s]
		size = sizeHash[nodeName.to_s]
		rcvGroups[nodeName.to_s].exec("java -jar #{property.profileJarFile} #{wmIp} 7009 7010 --createAttributes #{size} #{profOpts} >#{property.profileLogFile} 2>&1")
		info "java -jar #{property.profileJarFile} #{wmIp} 7009 7010 --createAttributes #{size} #{profOpts} >#{property.profileLogFile} 2>&1"
		currWmCount += 1;
		break if currWmCount >= numWMs;
	}


	info "Waiting 5 seconds to mark start of stable rate"
	wait 5
	info "Starting #{property.mainWait} seconds at #{DateTime.now}"
	waitLeft = property.mainWait.to_s.to_i
	while(waitLeft > 0) do
		second = (waitLeft % 60).floor
		minute = (waitLeft/60).floor
		hour = (waitLeft/3600).floor
		info "Time remaining #{hour}:#{minute}:#{second}"
		if waitLeft < 60
			wait waitLeft
			waitLeft = 0
		else
			wait 60
			waitLeft -= 60
		end
	end

	info "Ending #{property.mainWait} seconds at #{DateTime.now}"
	info "Waiting 5 seconds to clear logging"
	wait 5
	info "Ready to shut down experiment"

	profRecvTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall java")
		info "killall java"
	}

	info "Waiting 15 seconds for receivers to log results"
	wait 15

	wmTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall #{property.sqliteWmBinary}")
		rcvGroups[nodeName.to_s].exec("killall #{property.mysqlWmBinary}")
	}

	suffix = property.receiver.to_s == "true" ? "" : "_nr";
	prefix = property.individual.to_s == "true" ? "i": "";
	scp = "\n";
	sizeHash.each {|node,size|
		unit = 'k';
		size /= 1000;
		if(size > 999)
			unit = 'm';
			size /= 1000;
		end
		scp += "mkdir #{prefix}#{size}#{unit}#{suffix}\n"
	}
	i=0
	profTopology.nodes.each{ |nodeName|
		size = sizeHash[nodeName.to_s];
		unit = 'k';
		size /= 1000;
		if(size > 999)
			unit = 'm';
			size /= 1000;
		end
		scp += "scp root@#{nodeName.to_s}:/profiler.log #{prefix}#{size}#{unit}#{suffix}/\n"
		i += 1;
		break if i >= numWMs;
	}

	wmTopology.nodes.each{ |nodeName|
		size = sizeHash[nodeName.to_s];
		unit = 'k';
		size /= 1000;
		if(size > 999)
			unit = 'm';
			size /= 1000;
		end
		scp += "scp root@#{nodeName.to_s}:/worldmodel.log #{prefix}#{size}#{unit}#{suffix}/\n";
	}

	i = 0

	if property.receiver.to_s == "true" 
		profRecvTopology.nodes.each{|nodeName|
			size = sizeHash[nodeName.to_s]
			unit = 'k';
			size /= 1000;
			if(size > 999)
				unit = 'm';
				size /= 1000;
			end
			scp += "scp root@#{nodeName.to_s}:/receiver.log #{prefix}#{size}#{unit}#{suffix}/\n"
			i += 1
			break if i >= numWMs;
		}
	end

	info "#{scp}"

end

allTopology.nodes.each { |nodeName|
	info "#{nodeName}"
	receiverGroups[nodeName.to_s] = defGroup(nodeName.to_s,nodeName.to_s)
	info "Created group #{nodeName}"
}

info "Awaiting ALL_UP event"

onEvent(:ALL_UP) do |event|
	info "Detected ALL_UP. Launching IP configuration."
	main(allTopology,receiverGroups,wmTopology,profTopology,profRecvTopology)
	Experiment.done
end

