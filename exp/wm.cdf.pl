#!/usr/bin/perl

use strict;
use warnings;
use DateTime::Format::Strptime;
use File::Basename;

my %hash_ins;

print "Generating Insert CDF/PDF CSVs\n";
# Enable buffer autoflush
$| = 1; 

my $numFile = scalar @ARGV;
my $fileIndex = 0;

foreach my $fn (@ARGV) {
  my $frac = ($fileIndex+1)/$numFile;

  print "\r[";
  for(my $i = 0; $i < 40; $i += 1){
    print (($i*.025) <= $frac ? '#' : ' ');
  }
  print '] ('.int($frac*100).'%)';
  $fileIndex += 1;

  $fn =~ s/(.+)\.[^.]+$/$fn/;

  %hash_ins = ();

#  my $basename = $1; # base filename for adding extensions
  my $first = 0;
  my $totalCount = 0;

  
  open my $file, $fn or die "Could not open $fn: $!"; # Try to open each file
#  $fn =~ /.*\.([0-9]{4}\-[0-9]{2}\-[0-9]{2})/; 
  my ($filename,$path) = fileparse($fn);
  $path =~ /([a-zA-Z0-9_]+)\/$/;
  my $savename = $1;

  while (my $line = <$file>){

    # Insert timing for world model
    # server/Insert/1: 258512
    if( $line =~ /^server\/Insert\/([0-9]+): ([0-9]+)$/ ){
      my $count = $1;
      my $time = int($2/1000);
      if(not $first){
        $first = 1;
        next;
      }

      if(not exists $hash_ins{$time}){
        $hash_ins{$time} = 0;
      }
      $hash_ins{$time} += 1;
      $totalCount += 1;
      next;
    }
  }
  close $file;
  open (OUTFILE, '>'.$savename.'-pdf.csv');
  foreach my $ts2 (sort {$a <=> $b} (keys(%hash_ins))) {
    print OUTFILE "$ts2,".($hash_ins{$ts2}/$totalCount)."\n";
  }
  close (OUTFILE);

  open (OUTFILE, '>'.$savename.'-cdf.csv');
  my $cumulative = 0;
  foreach my $ts2 (sort {$a <=> $b} (keys(%hash_ins))) {
    $cumulative += $hash_ins{$ts2};
    print OUTFILE "$ts2,".($cumulative/$totalCount)."\n";
  }
  close (OUTFILE);

}

print "\n";


