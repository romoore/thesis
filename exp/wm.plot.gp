#!/usr/bin/gnuplot
#set bmargin 2.5;

set xlabel 'World Model Size (No. of Attributes)' 
set xtics rotate
set ylabel 'Time ({/Symbol m}s)'
set key outside right top box width -1 height 2 enhanced spacing 1
set terminal postscript enhanced size 10.5in,5in color #font "Helvetica" 8; 
set datafile sep ','; 
set log xy;
set title "{/=15 World Model Performance: 1% Range Request}"
set output "wm.range01.eps";
set yrange [7000:2e6]; 
set xrange [9000:2.5e6];
plot 'worldmodel-range01-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-range01-boxes.csv' u 1:6 title '95 Percentile' w linespoints lc 1,\
     'worldmodel-range01-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-range01-boxes.csv' u 1:11 title '95 Percentile'  w linespoints lc 2


set title "{/=15 World Model Performance: 5% Range Request}"
set output "wm.range05.eps";
set yrange [30000:1e7]; 
plot 'worldmodel-range05-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars,\
     'worldmodel-range05-boxes.csv' u 1:($6+$7):8:9:($6-$7) title 'No Expirations' w candlesticks whiskerbars

set title "{/=15 World Model Performance: 10% Range Request}"
set output "wm.range10.eps";
set yrange [7e4:2e7]; 
plot 'worldmodel-range10-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars,\
     'worldmodel-range10-boxes.csv' u 1:($6+$7):8:9:($6-$7) title 'No Expirations' w candlesticks whiskerbars

set title "{/=15 World Model Performance: Expiring Attributes}"
set output "wm.expire.eps";
set yrange [*:*];
set xrange [*:*];
unset log y;
plot 'worldmodel-expire-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars

set title "{/=15 World Model Performance: Deleting Attributes}"
set output "wm.delete.eps";
set yrange [*:*];
set xrange [*:*];
unset log y;
plot 'worldmodel-delete-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars lc 1,\
     'worldmodel-delete-boxes.csv' u 1:6 title '99 Percentile' w linespoints lc 1,\
     'worldmodel-delete-boxes.csv' u 1:($7+$8):9:10:($7-$8) title 'No Expirations' w candlesticks whiskerbars lc 2,\
     'worldmodel-delete-boxes.csv' u 1:11 title '99 Percentile' w linespoints lc 2

set title "{/=15 World Model Performance: Regex Search 1}"
set output "wm.regex1.eps";
set yrange [*:*];
set xrange [*:*];
set log y
plot 'worldmodel-regex1-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars,\
     'worldmodel-regex1-boxes.csv' u 1:($6+$7):8:9:($6-$7) title 'No Expirations' w candlesticks whiskerbars

set title "{/=15 World Model Performance: Regex Search 2}"
set output "wm.regex2.eps";
set yrange [*:*];
set xrange [*:*];
plot 'worldmodel-regex2-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars,\
     'worldmodel-regex2-boxes.csv' u 1:($6+$7):8:9:($6-$7) title 'No Expirations' w candlesticks whiskerbars

set title "{/=15 World Model Performance: Regex Search 3}"
set output "wm.regex3.eps";
set yrange [*:*];
set xrange [*:*];
plot 'worldmodel-regex3-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars,\
     'worldmodel-regex3-boxes.csv' u 1:($6+$7):8:9:($6-$7) title 'No Expirations' w candlesticks whiskerbars

set title "{/=15 World Model Performance: Regex Search 4}"
set output "wm.regex4.eps";
set yrange [*:*];
set xrange [*:*];
plot 'worldmodel-regex4-boxes.csv' u 1:($2+$3):4:5:($2-$3) title '1-Minute Expirations' w candlesticks whiskerbars,\
     'worldmodel-regex4-boxes.csv' u 1:($6+$7):8:9:($6-$7) title 'No Expirations' w candlesticks whiskerbars

set title "{/=15 World Model Performance: 5% Snapshot (1-Minute Expirations)}"
set key outside right top box width 0 height 2 enhanced spacing 1
set xlabel 'Snapshot Position in Data Set (Percentage)'
set ylabel 'Query Time ({/Symbol m}s)'
set output "wm.snapshot.exp.eps"
set yrange [*:*];
set xrange [0:100];
unset log x;
set log y;
plot 'worldmodel-snapshot-exp-values.csv' u 1:2 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:3 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:4 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:5 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:6 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:6 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:7 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:8 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:9 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:10 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:11 title columnhead w linespoints,\
     'worldmodel-snapshot-exp-values.csv' u 1:12 title columnhead w linespoints

set title "{/=15 World Model Performance: 5% Snapshot (No Expirations)}"
set key outside right top box width 0 height 2 enhanced spacing 1
set xlabel 'Snapshot Position in Data Set (Percentage)'
set ylabel 'Query Time ({/Symbol m}s)'
set output "wm.snapshot.noexp.eps"
set yrange [*:*];
set xrange [0:100];
unset log x;
set log y;
plot 'worldmodel-snapshot-noexp-values.csv' u 1:2 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:3 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:4 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:5 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:6 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:6 title columnhead w linespoints,\
     'worldmodel-snapshot-noexp-values.csv' u 1:7 title columnhead w linespoints
