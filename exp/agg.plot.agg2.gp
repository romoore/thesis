#!/usr/bin/gnuplot
#set bmargin 2.5;

set title "{/=15 Aggregator Performance: Multiple Replay Sensors and No Solvers}"
set xlabel 'Time (seconds)' 
set xtics rotate
set ylabel 'Samples/sec'
set key outside right top box width -2 height 2 enhanced spacing 1
set terminal postscript enhanced size 10.5in,5in color #font "Helvetica" 8; 
set output "agg2.rate.eps";
set timefmt '%Y-%m-%d %H:%M:%S'; 
set datafile sep ','; 
set yrange [*:*]; 
set xrange [*:*];
plot 'agg2-3-10.csv' u 1:2 title '3 Sensors, 1X Speed' w lines lc 1 lt 1,\
     'agg2-3-50.csv' u 1:2 title '3 Sensors, 5X Speed' w lines lc 1 lt 2,\
     'agg2-3-100.csv' u 1:2 title '3 Sensors, 10X Speed' w lines lc 1 lt 3,\
     'agg2-3-150.csv' u 1:2 title '3 Sensors, 15X Speed' w lines lc 1 lt 4,\
     'agg2-6-10.csv' u 1:2 title '6 Sensors, 1X Speed' w lines lc 2 lt 1,\
     'agg2-6-50.csv' u 1:2 title '6 Sensors, 5X Speed' w lines lc 2 lt 2,\
     'agg2-6-100.csv' u 1:2 title '6 Sensors, 10X Speed' w lines lc 2 lt 3,\
     'agg2-6-150.csv' u 1:2 title '6 Sensors, 15X Speed' w lines lc 2 lt 4,\
     'agg2-9-10.csv' u 1:2 title '9 Sensors, 1X Speed' w lines lc 3 lt 1,\
     'agg2-9-50.csv' u 1:2 title '9 Sensors, 5X Speed' w lines lc 3 lt 2,\
     'agg2-9-100.csv' u 1:2 title '9 Sensors, 10X Speed' w lines lc 3 lt 3,\
     'agg2-9-150.csv' u 1:2 title '9 Sensors, 15X Speed' w lines lc 3 lt 4
set output "agg2.proc.eps";
set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set key outside top right  box width -2 height 2 enhanced spacing 1
plot 'agg2-3-10.csv' u 1:3 title '3 Sensors, 1X Speed' w lines lc 1 lt 1,\
     'agg2-3-50.csv' u 1:3 title '3 Sensors, 5X Speed' w lines lc 1 lt 2,\
     'agg2-3-100.csv' u 1:3 title '3 Sensors, 10X Speed' w lines lc 1 lt 3,\
     'agg2-3-150.csv' u 1:3 title '3 Sensors, 15X Speed' w lines lc 1 lt 4,\
     'agg2-6-10.csv' u 1:3 title '6 Sensors, 1X Speed' w lines lc 2 lt 1,\
     'agg2-6-50.csv' u 1:3 title '6 Sensors, 5X Speed' w lines lc 2 lt 2,\
     'agg2-6-100.csv' u 1:3 title '6 Sensors, 10X Speed' w lines lc 2 lt 3,\
     'agg2-6-150.csv' u 1:3 title '6 Sensors, 15X Speed' w lines lc 2 lt 4,\
     'agg2-9-10.csv' u 1:3 title '9 Sensors, 1X Speed' w lines lc 3 lt 1,\
     'agg2-9-50.csv' u 1:3 title '9 Sensors, 5X Speed' w lines lc 3 lt 2,\
     'agg2-9-100.csv' u 1:3 title '9 Sensors, 10X Speed' w lines lc 3 lt 3,\
     'agg2-9-150.csv' u 1:3 title '9 Sensors, 15X Speed' w lines lc 3 lt 4
set output "agg2.life.eps";
set log y
set yrange [100:30000]
set ylabel 'Sample Lifetime ({/Symbol m}s)'
set key outside top right  box width -2  height 2 enhanced spacing 1
plot 'agg2-3-10.csv' u 1:4 title '3 Sensors, 1X Speed' w lines lc 1 lt 1,\
     'agg2-3-50.csv' u 1:4 title '3 Sensors, 5X Speed' w lines lc 1 lt 2,\
     'agg2-3-100.csv' u 1:4 title '3 Sensors, 10X Speed' w lines lc 1 lt 3,\
     'agg2-3-150.csv' u 1:4 title '3 Sensors, 15X Speed' w lines lc 1 lt 4,\
     'agg2-6-10.csv' u 1:4 title '6 Sensors, 1X Speed' w lines lc 2 lt 1,\
     'agg2-6-50.csv' u 1:4 title '6 Sensors, 5X Speed' w lines lc 2 lt 2,\
     'agg2-6-100.csv' u 1:4 title '6 Sensors, 10X Speed' w lines lc 2 lt 3,\
     'agg2-6-150.csv' u 1:4 title '6 Sensors, 15X Speed' w lines lc 2 lt 4,\
     'agg2-9-10.csv' u 1:4 title '9 Sensors, 1X Speed' w lines lc 3 lt 1,\
     'agg2-9-50.csv' u 1:4 title '9 Sensors, 5X Speed' w lines lc 3 lt 2,\
     'agg2-9-100.csv' u 1:4 title '9 Sensors, 10X Speed' w lines lc 3 lt 3,\
     'agg2-9-150.csv' u 1:4 title '9 Sensors, 15X Speed' w lines lc 3 lt 4
unset log y

set title "{/=15 Aggregator Performance: 3 Replay Sensors no Solvers}"
set output "agg2.3.svr.eps"
set ylabel 'Service Rate (Samples per Second)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
plot 'agg2-percentiles-3sensor.csv' u 1:2 title columnhead w lp,\
     'agg2-percentiles-3sensor.csv' u 1:3 title columnhead w lp

set output "agg2.3.svp.eps"
set ylabel 'Sample Processing Time ({/Symbol m}s)'
plot 'agg2-percentiles-3sensor.csv' u 1:4 title columnhead w lp,\
     'agg2-percentiles-3sensor.csv' u 1:5 title columnhead w lp

set output "agg2.3.svl.eps"
set ylabel 'Sample Lifetime ({/Symbol m}s)'
plot 'agg2-percentiles-3sensor.csv' u 1:6 title columnhead w lp,\
     'agg2-percentiles-3sensor.csv' u 1:7 title columnhead w lp

set title "{/=15 Aggregator Performance: 6 Replay Sensors no Solvers}"
set output "agg2.6.svr.eps"
set ylabel 'Service Rate (Samples per Second)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
plot 'agg2-percentiles-6sensor.csv' u 1:2 title columnhead w lp,\
     'agg2-percentiles-6sensor.csv' u 1:3 title columnhead w lp

set output "agg2.6.svp.eps"
set ylabel 'Sample Processing Time ({/Symbol m}s)'
plot 'agg2-percentiles-6sensor.csv' u 1:4 title columnhead w lp,\
     'agg2-percentiles-6sensor.csv' u 1:5 title columnhead w lp

set output "agg2.6.svl.eps"
set ylabel 'Sample Lifetime ({/Symbol m}s)'
plot 'agg2-percentiles-6sensor.csv' u 1:6 title columnhead w lp,\
     'agg2-percentiles-6sensor.csv' u 1:7 title columnhead w lp

set title "{/=15 Aggregator Performance: 9 Replay Sensors no Solvers}"
set output "agg2.9.svr.eps"
set ylabel 'Service Rate (Samples per Second)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
plot 'agg2-percentiles-9sensor.csv' u 1:2 title columnhead w lp,\
     'agg2-percentiles-9sensor.csv' u 1:3 title columnhead w lp

set output "agg2.9.svp.eps"
set ylabel 'Sample Processing Time ({/Symbol m}s)'
plot 'agg2-percentiles-9sensor.csv' u 1:4 title columnhead w lp,\
     'agg2-percentiles-9sensor.csv' u 1:5 title columnhead w lp

set output "agg2.9.svl.eps"
set ylabel 'Sample Lifetime ({/Symbol m}s)'
plot 'agg2-percentiles-9sensor.csv' u 1:6 title columnhead w lp,\
     'agg2-percentiles-9sensor.csv' u 1:7 title columnhead w lp


set title "{/=15 Impact of Service Rate on Processing Time}"
set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set output "agg2.rate_vs.proc.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set xlabel 'Average Sample Inter-Arrival Delay ({/Symbol m}s)'
#set log x
set xrange [100:0]
set yrange [.25:.65]
plot 'agg1.rate_vs.csv' u 1:2:3 title '1 Sensor' w yerrorbars,\
     'agg2-3.rate_vs.csv' u 1:2:3 title '3 Sensors' w yerrorbars,\
     'agg2-6.rate_vs.csv' u 1:2:3 title '6 Sensors' w yerrorbars,\
     'agg2-9.rate_vs.csv' u 1:2:3 title '9 Sensors' w yerrorbars
unset log x

set output "agg2.rate_vs.life.eps"
set title "{/=15 Impact of Service Rate on Sample Lifetime}"
set ylabel 'Sample Lifetime ({/Symbol m}s)'
set yrange [100:16000]
set log y
plot 'agg1.rate_vs.csv' u 1:4:5 title '1 Sensor' w yerrorbars,\
     'agg2-3.rate_vs.csv' u 1:4:5 title '3 Sensors' w yerrorbars,\
     'agg2-6.rate_vs.csv' u 1:4:5 title '6 Sensors' w yerrorbars,\
     'agg2-9.rate_vs.csv' u 1:4:5 title '9 Sensors' w yerrorbars
unset log y
