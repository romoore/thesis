#!/usr/bin/perl

use strict;
use warnings;
use DateTime::Format::Strptime;


my %arr_ps;
my %arr_pt;
my %arr_lt;

foreach my $fn (@ARGV) {

  
  open my $file, $fn or die "Could not open $fn: $!";
  $fn =~ /.*\.([0-9]{4}\-[0-9]{2}\-[0-9]{2})/;
  my $dt = $1;
  my $ts = '';
  my $ps = '';
  my $pt = '';
  my $lt = '';
  my $firstTime = 0;
  my $parser = DateTime::Format::Strptime->new(pattern => '%Y-%m-%d %H:%M:%S');

  while (my $line = <$file>){
    # [19:13:47.772] - INFO - Timer-0/statstimer - Processed 1,200 samples since last report.
    # 04:37:36.217
    # if( $line =~ /^\[([0-9]{2}\:[0-9]{2}\:[0-9]{2}).*\].*Processed\s([0-9,]+)/ ){

    # [2013-11-01 13:12:47,389] INFO  Timer-0/statstimer - Processed 1,200 samples since last report.
    # 04:37:36.217
    if( $line =~ /^\[.*([0-9]{2}\:[0-9]{2}\:[0-9]{2}).*\].*Processed\s([0-9,]+)/ ){
      my $temp = "$dt $1";
      $ts = $parser->parse_datetime($temp);
      if(0 == $firstTime){
        $firstTime = $ts->epoch();
        $ts = 0;
      }else {
        $ts = $ts->epoch() - $firstTime;
      }
      $ps = $2;
      $ps =~ tr/,//d;
      next;
    }

    # Avg. Process Time: 36,744.461 ns
    if($line =~ /^\sAvg\.\sProcess\sTime\:\s([0-9,]+\.[0-9]{3})/ ){
      $pt = $1;
      $pt =~ tr/,//d;
      next;
    }
    # Avg. Sample Lifetime: 0.078 ms
    if($line =~ /^\sAvg\.\sSample\sLifetime\:\s([0-9,]+\.[0-9]{3})/ ){
      $lt = $1;
      $lt =~ tr/,//d;
      $arr_ps{$ts} = $ps;
      $arr_pt{$ts} = $pt/1000;
      $arr_lt{$ts} = $lt*1000;
      $ts = $pt = $lt = $ps =  '';
      next;
    }
    
  }
  close $file;
}


foreach my $ts (sort {$a <=> $b} (keys(%arr_ps))) {
  print "$ts,$arr_ps{$ts},$arr_pt{$ts},$arr_lt{$ts}\n";
}
