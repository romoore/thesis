#!/usr/bin/gnuplot
#set bmargin 2.5;
set key inside right bottom box width 2 height 2 enhanced spacing 2

set title "{/=15 Aggregator Performance: Recording Solver Then No Solver}"
set xlabel 'Time (seconds)' 
set xtics rotate
set ylabel 'Samples/sec'
set terminal postscript enhanced #size 7in,3in color #font "Helvetica" 8; 
set output "aggregator.log.2013-11-01.rate.eps"; 
set timefmt '%Y-%m-%d %H:%M:%S'; 
set datafile sep ','; 
set yrange [*:*]; 
set xrange [*:*];
set arrow from 5400,11300 to 4920,11200 lc rgb 'red';
set label "Solver disconnected" at 5400,11300 font "Anonymous Pro,14"
plot 'aggregator.log.2013-11-01.dat' u 1:2 title '10-second Sampling' w lines
#     'aggregator.log.2013-11-01.dat' u 1:2:(1.0) smooth acsplines title 'Approximation Curve' w lines ls 1
set title "{/=15 Aggregator Performance: Recording Solver and No Solvers}"
set output "aggregator.log.2013-11-01.proc-life.eps";
set ylabel 'Time per Sample ({/Symbol m}s)'
set key inside top right  box width 2 height 2 enhanced spacing 2
set arrow from 5200,140 to 4920,160 lc rgb 'red';
set label "Solver disconnected" at 5200,140 font "Anonymous Pro,14"
plot 'aggregator.log.2013-11-01.dat' u 1:3 title 'Processing Time' w lines, \
     'aggregator.log.2013-11-01.dat' u 1:4 title 'Sample Lifetime' w lines ls 10 
