#!/usr/bin/gnuplot
#set bmargin 2.5;

set title "{/=15 Aggregator Performance: Replay Sensor and No Solvers}"
set xlabel 'Time (seconds)' 
set xtics rotate
set ylabel 'Samples/sec'
set key outside right top box width -1 height 2 enhanced spacing 1
set terminal postscript enhanced size 10.5in,5in color #font "Helvetica" 8; 
set output "agg1.rate.eps";
set timefmt '%Y-%m-%d %H:%M:%S'; 
set datafile sep ','; 
set yrange [*:*]; 
set xrange [*:*];
plot 'agg1-20.csv' u 1:2 title '2X Replay Speed' w lines,\
     'agg1-40.csv' u 1:2 title '4X Replay Speed' w lines,\
     'agg1-60.csv' u 1:2 title '6X Replay Speed' w lines,\
     'agg1-80.csv' u 1:2 title '8X Replay Speed' w lines,\
     'agg1-100.csv' u 1:2 title '10X Replay Speed' w lines,\
     'agg1-120.csv' u 1:2 title '12X Replay Speed' w lines,\
     'agg1-140.csv' u 1:2 title '14X Replay Speed' w lines,\
     'agg1-160.csv' u 1:2 title '16X Replay Speed' w lines,\
     'agg1-180.csv' u 1:2 title '18X Replay Speed' w lines,\
     'agg1-200.csv' u 1:2 title '20X Replay Speed' w lines
set output "agg1.proc.eps";
set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set key outside top right  box width -1 height 2 enhanced spacing 1
plot 'agg1-20.csv' u 1:3 title '2X Replay Speed' w lines,\
     'agg1-40.csv' u 1:3 title '4X Replay Speed' w lines,\
     'agg1-60.csv' u 1:3 title '6X Replay Speed' w lines,\
     'agg1-80.csv' u 1:3 title '8X Replay Speed' w lines,\
     'agg1-100.csv' u 1:3 title '10X Replay Speed' w lines,\
     'agg1-120.csv' u 1:3 title '12X Replay Speed' w lines,\
     'agg1-140.csv' u 1:3 title '14X Replay Speed' w lines,\
     'agg1-160.csv' u 1:3 title '16X Replay Speed' w lines,\
     'agg1-180.csv' u 1:3 title '18X Replay Speed' w lines,\
     'agg1-200.csv' u 1:3 title '20X Replay Speed' w lines
set output "agg1.life.eps";
set ylabel 'Sample Lifetime ({/Symbol m}s)'
set key outside top right  box width -1 height 2 enhanced spacing 1
plot 'agg1-20.csv' u 1:4 title '2X Replay Speed' w lines,\
     'agg1-40.csv' u 1:4 title '4X Replay Speed' w lines,\
     'agg1-60.csv' u 1:4 title '6X Replay Speed' w lines,\
     'agg1-80.csv' u 1:4 title '8X Replay Speed' w lines,\
     'agg1-100.csv' u 1:4 title '10X Replay Speed' w lines,\
     'agg1-120.csv' u 1:4 title '12X Replay Speed' w lines,\
     'agg1-140.csv' u 1:4 title '14X Replay Speed' w lines,\
     'agg1-160.csv' u 1:4 title '16X Replay Speed' w lines,\
     'agg1-180.csv' u 1:4 title '18X Replay Speed' w lines,\
     'agg1-200.csv' u 1:4 title '20X Replay Speed' w lines


set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set xlabel 'Service Rate (Samples per second)'
set key inside top right  box width -1 height 2 enhanced spacing 1
set output "agg1.rvp.eps"
plot 'agg1-rate-v-time.csv' u 1:2 title '50 Percentile' w lines


set output "agg1.svr.eps"
set ylabel 'Service Rate (Samples per Second)'
set xlabel 'Sensor Speed (Multiple of Recorded)'
set key inside bottom right box width -1 height 2 enhanced spacing 1
plot 'agg1-percentiles.csv' u 1:2 title columnhead w l,\
     'agg1-percentiles.csv' u 1:3 title columnhead w lp 

set output "agg1.svp.eps"
set key inside top right box width 2 height 2 enhanced spacing 1
set ylabel 'Sample Processing Time ({/Symbol m}s)'
plot 'agg1-percentiles.csv' u 1:4 title columnhead w lines,\
     'agg1-percentiles.csv' u 1:5 title columnhead w lines

set output "agg1.svl.eps"
set ylabel 'Sample Lifetime ({/Symbol m}s)'
set key inside bottom right box width 2 height 2 enhanced spacing 1
plot 'agg1-percentiles.csv' u 1:6 title columnhead w lines,\
     'agg1-percentiles.csv' u 1:7 title columnhead w lines

set output "agg1.rate_vs.proc.eps"
set key inside top left box width 2 height 2 enhanced spacing 1
set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set xlabel 'Average Sample Inter-Arrival Delay({/Symbol m}s)'
set xrange [130:0]
set yrange [0:1]
plot 'agg1.rate_vs.csv' u 1:2:3 title 'Processing Time' w yerrorbars
set output "agg1.rate_vs.life.eps"
set yrange [0:1000]
plot 'agg1.rate_vs.csv' u 1:4:5 title 'Sample Lifetime' w yerrorbars
