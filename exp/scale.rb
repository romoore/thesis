# Experimental properties
# Override at command line like this:
# omf exec scale.rb -- --numSensors 5 --replaySpeed 2.5

defProperty('numAggregators',1,"Number of Aggregators (1 per node)")
defProperty('numSensors',1,"Number of replay senseors (1 per node)")
defProperty('numSolvers',1,"Number of no-op solvers (1 per node)")
defProperty('sensorFile','/root/empty80.gss','Name of playback file')
defProperty('replaySpeed',1.0,'Playback speed')
defProperty('mainWait',600,'How long to wait to collect data')
defProperty('sel5Solver',false,'Whether to use select-5 solver (true) or no-op (default)')

# Constants, file names, and paths
defProperty('aggJarFile','/root/aggregator.jar','Filename for aggregator jar')
defProperty('aggLogFile', '/root/aggregator.log', 'Logfile for aggregator')
defProperty('aggOpts','','Options for aggregator')
defProperty('sensJarFile','/root/recording.jar','Filename for recording sensor')
defProperty('sensLogFile','/root/recording.log','Logfile for sensor')
defProperty('sensClass','com.owlplatform.solver.recording.ReplaySensor','Java class name for replay sensor.')
defProperty('noopSolvClass','com.owlplatform.solver.recording.NoOpSolver','Java class name for no-op solver.')
defProperty('sel5SolvClass','com.owlplatform.solver.recording.Select5Solver','Java class name for select-5 solver.')


#allTopology = Topology['system:topo:imaged']
allTopology = defTopology('nodes','node1-1.grid.orbit-lab.org,node1-2.grid.orbit-lab.org,node10-11.grid.orbit-lab.org,node10-14.grid.orbit-lab.org,node10-17.grid.orbit-lab.org,node10-7.grid.orbit-lab.org,node11-1.grid.orbit-lab.org,node11-11.grid.orbit-lab.org,node11-17.grid.orbit-lab.org,node11-7.grid.orbit-lab.org,node13-13.grid.orbit-lab.org,node13-8.grid.orbit-lab.org,node14-10.grid.orbit-lab.org,node14-7.grid.orbit-lab.org,node5-5.grid.orbit-lab.org,node6-15.grid.orbit-lab.org,node6-6.grid.orbit-lab.org,node7-10.grid.orbit-lab.org,node7-14.grid.orbit-lab.org,node7-7.grid.orbit-lab.org,node8-13.grid.orbit-lab.org,node8-18.grid.orbit-lab.org,node8-3.grid.orbit-lab.org,node8-8.grid.orbit-lab.org')
aggTopology = defTopology('aggregators','node1-1.grid.orbit-lab.org,node1-2.grid.orbit-lab.org')
solvTopology = defTopology('solvers','node10-11.grid.orbit-lab.org,node10-14.grid.orbit-lab.org,node10-17.grid.orbit-lab.org,node10-7.grid.orbit-lab.org,node11-1.grid.orbit-lab.org,node11-11.grid.orbit-lab.org,node11-17.grid.orbit-lab.org,node11-7.grid.orbit-lab.org,node13-13.grid.orbit-lab.org,node13-8.grid.orbit-lab.org')
sensTopology = defTopology('sensors','node14-10.grid.orbit-lab.org,node14-7.grid.orbit-lab.org,node5-5.grid.orbit-lab.org,node6-15.grid.orbit-lab.org,node6-6.grid.orbit-lab.org,node7-10.grid.orbit-lab.org,node7-14.grid.orbit-lab.org,node7-7.grid.orbit-lab.org,node8-13.grid.orbit-lab.org,node8-18.grid.orbit-lab.org,node8-3.grid.orbit-lab.org,node8-8.grid.orbit-lab.org')

receiverGroups = Hash.new



def main(topology, rcvGroups, aggTopology, sensTopology, solvTopology) 
	nodeIpMap = 
	{
		# Aggregators
		'node1-1.grid.orbit-lab.org' => '192.168.1.1',
		'node1-2.grid.orbit-lab.org' => '192.168.1.2',
		# Solvers
		'node10-11.grid.orbit-lab.org' => '192.168.1.50',
		'node10-14.grid.orbit-lab.org' => '192.168.1.51',
		'node10-17.grid.orbit-lab.org' => '192.168.1.52',
		'node10-7.grid.orbit-lab.org' => '192.168.1.53',
		'node11-1.grid.orbit-lab.org' => '192.168.1.54',
		'node11-11.grid.orbit-lab.org' => '192.168.1.55',
		'node11-17.grid.orbit-lab.org' => '192.168.1.56',
		'node11-7.grid.orbit-lab.org' => '192.168.1.57',
		'node13-13.grid.orbit-lab.org' => '192.168.1.58',
		'node13-8.grid.orbit-lab.org' => '192.168.1.59',
		# Sensors
		'node14-10.grid.orbit-lab.org' => '192.168.1.100',
		'node14-7.grid.orbit-lab.org' => '192.168.1.101',
		'node5-5.grid.orbit-lab.org' => '192.168.1.102',
		'node6-15.grid.orbit-lab.org' => '192.168.1.103',
		'node6-6.grid.orbit-lab.org' => '192.168.1.104',
		'node7-10.grid.orbit-lab.org' => '192.168.1.105',
		'node7-14.grid.orbit-lab.org' => '192.168.1.106',
		'node7-7.grid.orbit-lab.org' => '192.168.1.107',
		'node8-13.grid.orbit-lab.org' => '192.168.1.108',
		'node8-18.grid.orbit-lab.org' => '192.168.1.109',
		'node8-3.grid.orbit-lab.org' => '192.168.1.110',
		'node8-8.grid.orbit-lab.org' => '192.168.1.111'

	}
info "##### SETTINGS #####"
	info "numAggregators: #{property.numAggregators}"
	info "numSensors: #{property.numSensors}"
	info "numSolvers: #{property.numSolvers}"
	info "sensorFile: #{property.sensorFile}"
	info "sel5Solver: #{property.sel5Solver}"
	info "replaySpeed: #{property.replaySpeed}"
	info "mainWait: #{property.mainWait}"
	info "####################"


	topology.nodes.each { |nodeName|
		rcvGroups[nodeName.to_s].net.e0.ip = nodeIpMap[nodeName.to_s]
		info "Set #{nodeName} IP address #{nodeIpMap[nodeName.to_s]}"
	}

	info "Waiting 10 seconds for networking to resolve"
	wait 10
	info "Starting #{property.numAggregators} aggregators"

	idx = 0

	aggTopology.nodes.each { |nodeName|
		if idx >= property.numAggregators
			break
		end
		wait 1
		rcvGroups[nodeName.to_s].exec("java -mx64m -XX:+HeapDumpOnOutOfMemoryError -jar #{property.aggJarFile} #{property.aggOpts} >#{property.aggLogFile}")
		info "Started aggregator on #{nodeName}"
		idx += 1
	}

	info "Waiting 10 seconds for aggregators to start/stabilize"
	wait 10
	info "Starting #{property.numSensors} sensors"

	idx = 0

	sensTopology.nodes.each{ |nodeName|
		if idx >= property.numSensors
			break;
		end
		wait 1
		rcvGroups[nodeName.to_s].exec("java -cp #{property.sensJarFile} #{property.sensClass} 192.168.1.1 7007 #{property.sensorFile} -X #{property.replaySpeed}")
		info "#{nodeName}: java -cp #{property.sensJarFile} #{property.sensClass} 192.168.1.1 7007 #{property.sensorFile} -X #{property.replaySpeed}"
		idx += 1
	}

	info "Starting #{property.numSolvers} solvers"

	idx = 0

	sClass = (property.sel5Solver.to_s == "true") ? property.sel5SolvClass : property.noopSolvClass

	solvTopology.nodes.each{ |nodeName|
		if idx >= property.numSolvers
			break;
		end
		wait 1
		rcvGroups[nodeName.to_s].exec("java -cp #{property.sensJarFile} #{sClass} 192.168.1.1 7008")
		info "#{nodeName}: java -cp #{property.sensJarFile} #{sClass} 192.168.1.1 7008"
		idx += 1
	}

	info "Waiting 30 seconds to mark start of stable rate"
	wait 30
	info "Starting #{property.mainWait} seconds at #{DateTime.now}"
	wait property.mainWait
	info "Ending #{property.mainWait} seconds at #{DateTime.now}"
	info "Waiting 30 seconds to clear logging"
	wait 30
	info "Ready to shut down experiment"

	sensTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall java")
	}

  wait 1

	sensTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall -9 java")
		info "#{nodeName}: killall -9 java"
	}

	solvTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall java")
	}

  wait 1

	solvTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall -9 java")
		info "#{nodeName}: killall -9 java"
	}

	aggTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall java")
	}

  wait 1

	aggTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall -9 java")
		info "#{nodeName}: killall -9 java"
	}

end

allTopology.nodes.each { |nodeName|
	info "#{nodeName}"
	receiverGroups[nodeName.to_s] = defGroup(nodeName.to_s,nodeName.to_s)
	info "Created group #{nodeName}"
}

info "Awaiting ALL_UP event"

onEvent(:ALL_UP) do |event|
	info "Detected ALL_UP. Launching IP configuration."
	main(allTopology,receiverGroups,aggTopology,sensTopology,solvTopology)
	Experiment.done
end

