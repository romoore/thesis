#!/usr/bin/perl

use strict;
use warnings;
use DateTime::Format::Strptime;

my %arr_ps;
my %arr_pt;
my %arr_lt;

my @agg_max;
my %all_max = ();


foreach my $fn (@ARGV) {
  $fn =~ s/(.+)\.[^.]+$/$fn/;

  %arr_ps = ();
  %arr_pt = ();
  %arr_lt = ();

  @agg_max = (-1,0,0,'nofile.csv');

  my $basename = $1; # base filename for adding extensions

  
  open my $file, $fn or die "Could not open $fn: $!"; # Try to open each file
#  $fn =~ /.*\.([0-9]{4}\-[0-9]{2}\-[0-9]{2})/; 
  my $ts = ''; # Timestamp for each output value
  my $ps = ''; # Processed samples value
  my $pt = ''; # Processing time per sample (ns)
  my $lt = ''; # Sample lifetime average (ms)
  my $firstTime = 0; # First timestamp encountered, used to calculate offsets
  my $parser = DateTime::Format::Strptime->new(pattern => '%Y-%m-%d %H:%M:%S');
  my $dt = '';

  while (my $line = <$file>){
    # [19:13:47.772] - INFO - Timer-0/statstimer - Processed 1,200 samples since last report.
    # 04:37:36.217
    # if( $line =~ /^\[([0-9]{2}\:[0-9]{2}\:[0-9]{2}).*\].*Processed\s([0-9,]+)/ ){

    # [2013-11-01 13:12:47,389] INFO  Timer-0/statstimer - Processed 1,200 samples since last report.
    # 04:37:36.217
    if( $line =~ /^\[.*([0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}).*\].*Processed\s([0-9,]+)/ ){
      my $temp = "$1";
      $ts = $parser->parse_datetime($temp);
      if(0 == $firstTime){
        $firstTime = $ts->epoch();
        $ts = 0;
        $dt = $ts;
      }else {
        $ts = $ts->epoch() - $firstTime;
      }
      $ps = $2;
      $ps =~ tr/,//d;
      next;
    }

    # Avg. Process Time: 36,744.461 ns
    if($line =~ /^\sAvg\.\sProcess\sTime\:\s([0-9,]+\.[0-9]{3})/ ){
      $pt = $1;
      $pt =~ tr/,//d;
      next;
    }
    # Avg. Sample Lifetime: 0.078 ms
    if($line =~ /^\sAvg\.\sSample\sLifetime\:\s([0-9,]+\.[0-9]{3})/ ){
      $lt = $1;
      $lt =~ tr/,//d;
      $arr_ps{$ts} = $ps/10; # Output value is per 10 seconds
      $arr_pt{$ts} = $pt/1000; # Output is in ns, convert to us
      $arr_lt{$ts} = $lt*1000; # Output is in ms, convert to us
      if(($ps/10) > $agg_max[0]) {
        @agg_max = ($ps/10, $pt/1000, $lt*1000);
      }
      $ts = $pt = $lt = $ps =  '';
      next;
    }
    
  }
  close $file;
  open (OUTFILE, '>'.$basename.'.csv');
  foreach my $ts2 (sort {$a <=> $b} (keys(%arr_ps))) {
    print OUTFILE "$ts2,$arr_ps{$ts2},$arr_pt{$ts2},$arr_lt{$ts2}\n";
  }
  close (OUTFILE);

  @{$all_max{$basename}} = ($agg_max[0],$agg_max[1],$agg_max[2]);
}

open (OUTFILE, '>all-max.csv');
foreach my $bs (sort {$a cmp $b} (keys(%all_max))) {
  my @a1 = @{$all_max{$bs}};
  print OUTFILE "$bs,$a1[0],$a1[1],$a1[2]\n";
}
close (OUTFILE);

