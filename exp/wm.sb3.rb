# Experimental properties
# Override at command line like this:
# omf exec scale.rb -- --numSensors 5 --replaySpeed 2.5

defProperty('useSQL','false','')
defProperty('numAttributes',1000,'')
defProperty('expire','false','')
defProperty('mainWait',300,'')
defProperty('individual','false','')
defProperty('receiver','true','')

# Constants, file names, and paths
defProperty('sqliteWmBinary','/usr/local/bin/owl/sqlite3_world_model_server','')
defProperty('mysqlWmBinary','/usr/local/bin/owl/mysql_world_model_server','')
defProperty('wmLogFile', '/worldmodel.log', '')
defProperty('sqlUser', 'root','')
defProperty('sqlPass','root','')
defProperty('profileJarFile','/wm-profiler.jar','')
defProperty('profileLogFile', '/profiler.log', '')
defProperty('profileRcvClass','com.owlplatform.wmprofile.Receiver','')
defProperty('receiverLogFile', '/receiver.log', '')


#allTopology = Topology['system:topo:imaged']
allTopology = defTopology('nodes','node1-1.sb3.orbit-lab.org,node1-2.sb3.orbit-lab.org')
wmTopology = defTopology('worldmodels','node1-1.sb3.orbit-lab.org')
profTopology = defTopology('profilers','node1-2.sb3.orbit-lab.org')
profRecvTopology = defTopology('profiler-receivers','node1-2.sb3.orbit-lab.org')

receiverGroups = Hash.new



def main(topology, rcvGroups, wmTopology, profTopology, profRecvTopology) 
	nodeIpMap = 
	{
		# World Model
		'node1-1.sb3.orbit-lab.org' => '192.168.1.1',
		# Profilers
		'node1-2.sb3.orbit-lab.org' => '192.168.1.2'

	}

	info "##### SETTINGS #####"
	info "Use SQL? #{property.useSQL}"
	info "No. Attrs: #{property.numAttributes}"
	info "Expire? #{property.expire}"
	info "####################"


	topology.nodes.each { |nodeName|
		rcvGroups[nodeName.to_s].net.e0.ip = nodeIpMap[nodeName.to_s]
		info "Set #{nodeName} IP address #{nodeIpMap[nodeName.to_s]}"
	}

	info "Waiting 5 seconds for networking to resolve"
	wait 5 
	info "Starting world model"

	idx = 0

	binary = property.useSQL.to_s == "true" ? property.mysqlWmBinary : property.sqliteWmBinary

	info "Clearing world model files"
	wmTopology.nodes.each { |nodeName|
		rcvGroups[nodeName.to_s].exec("rm /world_*")
	}

	wmTopology.nodes.each { |nodeName|
		rcvGroups[nodeName.to_s].exec("#{binary} >#{property.wmLogFile}")
		info "Started world model on #{nodeName}"
	}

	info "Waiting 5 seconds for world model to start/stabilize"
	wait 5
	if property.receiver.to_s == "true"
		info "Starting receiver"

		profRecvTopology.nodes.each{ |nodeName|
			rcvGroups[nodeName.to_s].exec("java -cp #{property.profileJarFile} #{property.profileRcvClass} 192.168.1.1 7010 >#{property.receiverLogFile}")
			info "java -cp #{property.profileJarFile} #{property.profileRcvClass} 192.168.1.1 7010 >#{property.receiverLogFile}"
		}

		info "Waiting 5 seconds between receiver/profiler"
		wait 5
	end
	info "Starting profiler"

	profOpts = property.expire.to_s == "true" ? "--expire" : ""
	if (property.individual.to_s == "true")
		profOpts += " --individual"
	end

	profTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("java -jar #{property.profileJarFile} 192.168.1.1 7009 7010 --createAttributes #{property.numAttributes} #{profOpts} >#{property.profileLogFile}")
		info "java -jar #{property.profileJarFile} 192.168.1.1 7009 7010 --createAttributes #{property.numAttributes} #{profOpts} >#{property.profileLogFile}"
	}


	info "Waiting 5 seconds to mark start of stable rate"
	wait 5
	info "Starting #{property.mainWait} seconds at #{DateTime.now}"
	wait property.mainWait
	info "Ending #{property.mainWait} seconds at #{DateTime.now}"
	info "Waiting 5 seconds to clear logging"
	wait 5
	info "Ready to shut down experiment"

	wmTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall #{property.sqliteWmBinary}")
		rcvGroups[nodeName.to_s].exec("killall #{property.mysqlWmBinary}")
	}
	profRecvTopology.nodes.each{ |nodeName|
		rcvGroups[nodeName.to_s].exec("killall java")
		info "killall java"
	}

end

allTopology.nodes.each { |nodeName|
	info "#{nodeName}"
	receiverGroups[nodeName.to_s] = defGroup(nodeName.to_s,nodeName.to_s)
	info "Created group #{nodeName}"
}

info "Awaiting ALL_UP event"

onEvent(:ALL_UP) do |event|
	info "Detected ALL_UP. Launching IP configuration."
	main(allTopology,receiverGroups,wmTopology,profTopology,profRecvTopology)
	Experiment.done
end

