#!/usr/bin/gnuplot
#set bmargin 2.5;

set title "{/=15 Aggregator Performance: 5 Replay Sensors and \"All Packets\" Solvers}"
set xlabel 'Time (seconds)' 
set xtics rotate
set ylabel 'Samples/sec'
set key outside right top box width -2 height 2 enhanced spacing 1
set terminal postscript enhanced size 10.5in,5in color #font "Helvetica" 8; 
set output "agg3.rate.eps";
set timefmt '%Y-%m-%d %H:%M:%S'; 
set datafile sep ','; 
set yrange [*:*]; 
set xrange [*:*];
plot 'agg3-1-10.csv' u 1:2 title '1 Solvers, 1X Speed' w lines lc 1 lt 1,\
     'agg3-1-50.csv' u 1:2 title '1 Solvers, 5X Speed' w lines lc 1 lt 2,\
     'agg3-1-100.csv' u 1:2 title '1 Solvers, 10X Speed' w lines lc 1 lt 3,\
     'agg3-1-150.csv' u 1:2 title '1 Solvers, 15X Speed' w lines lc 1 lt 4,\
     'agg3-3-10.csv' u 1:2 title '3 Solvers, 1X Speed' w lines lc 2 lt 1,\
     'agg3-3-50.csv' u 1:2 title '3 Solvers, 5X Speed' w lines lc 2 lt 2,\
     'agg3-3-100.csv' u 1:2 title '3 Solvers, 10X Speed' w lines lc 2 lt 3,\
     'agg3-3-150.csv' u 1:2 title '3 Solvers, 15X Speed' w lines lc 2 lt 4,\
     'agg3-6-10.csv' u 1:2 title '6 Solvers, 1X Speed' w lines lc 3 lt 1,\
     'agg3-6-50.csv' u 1:2 title '6 Solvers, 5X Speed' w lines lc 3 lt 2,\
     'agg3-6-100.csv' u 1:2 title '6 Solvers, 10X Speed' w lines lc 3 lt 3,\
     'agg3-6-150.csv' u 1:2 title '6 Solvers, 15X Speed' w lines lc 3 lt 4,\
     'agg3-9-10.csv' u 1:2 title '9 Solvers, 1X Speed' w lines lc 4 lt 1,\
     'agg3-9-50.csv' u 1:2 title '9 Solvers, 5X Speed' w lines lc 4 lt 2,\
     'agg3-9-100.csv' u 1:2 title '9 Solvers, 10X Speed' w lines lc 4 lt 3,\
     'agg3-9-150.csv' u 1:2 title '9 Solvers, 15X Speed' w lines lc 4 lt 4
set output "agg3.proc.eps";
set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set key outside top right  box width -2 height 2 enhanced spacing 1
plot 'agg3-1-10.csv' u 1:3 title '1 Solvers, 1X Speed' w lines lc 1 lt 1,\
     'agg3-1-50.csv' u 1:3 title '1 Solvers, 5X Speed' w lines lc 1 lt 2,\
     'agg3-1-100.csv' u 1:3 title '1 Solvers, 10X Speed' w lines lc 1 lt 3,\
     'agg3-1-150.csv' u 1:3 title '1 Solvers, 15X Speed' w lines lc 1 lt 4,\
     'agg3-3-10.csv' u 1:3 title '3 Solvers, 1X Speed' w lines lc 2 lt 1,\
     'agg3-3-50.csv' u 1:3 title '3 Solvers, 5X Speed' w lines lc 2 lt 2,\
     'agg3-3-100.csv' u 1:3 title '3 Solvers, 10X Speed' w lines lc 2 lt 3,\
     'agg3-3-150.csv' u 1:3 title '3 Solvers, 15X Speed' w lines lc 2 lt 4,\
     'agg3-6-10.csv' u 1:3 title '6 Solvers, 1X Speed' w lines lc 3 lt 1,\
     'agg3-6-50.csv' u 1:3 title '6 Solvers, 5X Speed' w lines lc 3 lt 2,\
     'agg3-6-100.csv' u 1:3 title '6 Solvers, 10X Speed' w lines lc 3 lt 3,\
     'agg3-6-150.csv' u 1:3 title '6 Solvers, 15X Speed' w lines lc 3 lt 4,\
     'agg3-9-10.csv' u 1:3 title '9 Solvers, 1X Speed' w lines lc 4 lt 1,\
     'agg3-9-50.csv' u 1:3 title '9 Solvers, 5X Speed' w lines lc 4 lt 2,\
     'agg3-9-100.csv' u 1:3 title '9 Solvers, 10X Speed' w lines lc 4 lt 3,\
     'agg3-9-150.csv' u 1:3 title '9 Solvers, 15X Speed' w lines lc 4 lt 4
set output "agg3.life.eps";
set ylabel 'Sample Lifetime ({/Symbol m}s)'
set log y
set yrange [100:80000]
set key outside top right  box width -2 height 2 enhanced spacing 1
plot 'agg3-1-10.csv' u 1:4 title '1 Solvers, 1X Speed' w lines lc 1 lt 1,\
     'agg3-1-50.csv' u 1:4 title '1 Solvers, 5X Speed' w lines lc 1 lt 2,\
     'agg3-1-100.csv' u 1:4 title '1 Solvers, 10X Speed' w lines lc 1 lt 3,\
     'agg3-1-150.csv' u 1:4 title '1 Solvers, 15X Speed' w lines lc 1 lt 4,\
     'agg3-3-10.csv' u 1:4 title '3 Solvers, 1X Speed' w lines lc 2 lt 1,\
     'agg3-3-50.csv' u 1:4 title '3 Solvers, 5X Speed' w lines lc 2 lt 2,\
     'agg3-3-100.csv' u 1:4 title '3 Solvers, 10X Speed' w lines lc 2 lt 3,\
     'agg3-3-150.csv' u 1:4 title '3 Solvers, 15X Speed' w lines lc 2 lt 4,\
     'agg3-6-10.csv' u 1:4 title '6 Solvers, 1X Speed' w lines lc 3 lt 1,\
     'agg3-6-50.csv' u 1:4 title '6 Solvers, 5X Speed' w lines lc 3 lt 2,\
     'agg3-6-100.csv' u 1:4 title '6 Solvers, 10X Speed' w lines lc 3 lt 3,\
     'agg3-6-150.csv' u 1:4 title '6 Solvers, 15X Speed' w lines lc 3 lt 4,\
     'agg3-9-10.csv' u 1:4 title '9 Solvers, 1X Speed' w lines lc 4 lt 1,\
     'agg3-9-50.csv' u 1:4 title '9 Solvers, 5X Speed' w lines lc 4 lt 2,\
     'agg3-9-100.csv' u 1:4 title '9 Solvers, 10X Speed' w lines lc 4 lt 3,\
     'agg3-9-150.csv' u 1:4 title '9 Solvers, 15X Speed' w lines lc 4 lt 4
unset log y


set title "{/=15 Aggregator Performance: 5 Replay Sensors and 1 \"All Packets\" Solvers}"
set xlabel 'Sensor Speed (Multiple of Recorded)'
set ylabel 'Service Rate (Samples per Second)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set output "agg3.1.svr.eps"
plot 'agg3-percentiles-1solver.csv' u 1:2 title columnhead w lp,\
     'agg3-percentiles-1solver.csv' u 1:3 title columnhead w lp

set ylabel 'Sample Processing Time ({/Symbol m}s)'
set key inside top right  box width 2 height 2 enhanced spacing 1
set output "agg3.1.svp.eps"
plot 'agg3-percentiles-1solver.csv' u 1:4 title columnhead w lp,\
     'agg3-percentiles-1solver.csv' u 1:5 title columnhead w lp

set ylabel 'Sample Lifetime ({/Symbol m}s)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set output "agg3.1.svl.eps"
plot 'agg3-percentiles-1solver.csv' u 1:6 title columnhead w lp,\
     'agg3-percentiles-1solver.csv' u 1:7 title columnhead w lp

set title "{/=15 Aggregator Performance: 5 Replay Sensors and 3 \"All Packets\" Solvers}"
set xlabel 'Sensor Speed (Multiple of Recorded)'
set ylabel 'Service Rate (Samples per Second)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set output "agg3.3.svr.eps"
plot 'agg3-percentiles-3solver.csv' u 1:2 title columnhead w lp,\
     'agg3-percentiles-3solver.csv' u 1:3 title columnhead w lp

set ylabel 'Sample Processing Time ({/Symbol m}s)'
set key inside top right  box width 2 height 2 enhanced spacing 1
set output "agg3.3.svp.eps"
plot 'agg3-percentiles-3solver.csv' u 1:4 title columnhead w lp,\
     'agg3-percentiles-3solver.csv' u 1:5 title columnhead w lp

set ylabel 'Sample Lifetime ({/Symbol m}s)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set output "agg3.3.svl.eps"
plot 'agg3-percentiles-3solver.csv' u 1:6 title columnhead w lp,\
     'agg3-percentiles-3solver.csv' u 1:7 title columnhead w lp

set title "{/=15 Aggregator Performance: 5 Replay Sensors and 6 \"All Packets\" Solvers}"
set xlabel 'Sensor Speed (Multiple of Recorded)'
set ylabel 'Service Rate (Samples per Second)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set output "agg3.6.svr.eps"
plot 'agg3-percentiles-6solver.csv' u 1:2 title columnhead w lp,\
     'agg3-percentiles-6solver.csv' u 1:3 title columnhead w lp

set ylabel 'Sample Processing Time ({/Symbol m}s)'
set key inside top right  box width 2 height 2 enhanced spacing 1
set output "agg3.6.svp.eps"
plot 'agg3-percentiles-6solver.csv' u 1:4 title columnhead w lp,\
     'agg3-percentiles-6solver.csv' u 1:5 title columnhead w lp

set ylabel 'Sample Lifetime ({/Symbol m}s)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set output "agg3.6.svl.eps"
plot 'agg3-percentiles-6solver.csv' u 1:6 title columnhead w lp,\
     'agg3-percentiles-6solver.csv' u 1:7 title columnhead w lp

set title "{/=15 Aggregator Performance: 5 Replay Sensors and 9 \"All Packets\" Solvers}"
set xlabel 'Sensor Speed (Multiple of Recorded)'
set ylabel 'Service Rate (Samples per Second)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set output "agg3.9.svr.eps"
plot 'agg3-percentiles-9solver.csv' u 1:2 title columnhead w lp,\
     'agg3-percentiles-9solver.csv' u 1:3 title columnhead w lp

set ylabel 'Sample Processing Time ({/Symbol m}s)'
set key inside top right  box width 2 height 2 enhanced spacing 1
set output "agg3.9.svp.eps"
plot 'agg3-percentiles-9solver.csv' u 1:4 title columnhead w lp,\
     'agg3-percentiles-9solver.csv' u 1:5 title columnhead w lp

set ylabel 'Sample Lifetime ({/Symbol m}s)'
set key inside bottom right  box width 2 height 2 enhanced spacing 1
set output "agg3.9.svl.eps"
plot 'agg3-percentiles-9solver.csv' u 1:6 title columnhead w lp,\
     'agg3-percentiles-9solver.csv' u 1:7 title columnhead w lp


set output "agg3.rate_vs.proc.eps"
set title "{/=15 Impact of Service Rate on Processing Time}"
set ylabel 'Processing Time per Sample ({/Symbol m}s)'
set key inside top left box width 2 height 2 enhanced spacing 1
set xlabel 'Average Sample Inter-Arrival Delay ({/Symbol m}s)'
set xrange [80:0]
set yrange [0:50]
plot 'agg3-1.rate_vs.csv' u 1:2:3 title '1 Solver' w yerrorbars,\
     'agg3-3.rate_vs.csv' u 1:2:3 title '3 Solvers' w yerrorbars,\
     'agg3-6.rate_vs.csv' u 1:2:3 title '6 Solvers' w yerrorbars,\
     'agg3-9.rate_vs.csv' u 1:2:3 title '9 Solvers' w yerrorbars

set output "agg3.rate_vs.life.eps"
set title "{/=15 Impact of Service Rate on Sample Lifetime}"
set ylabel 'Sample Lifetime ({/Symbol m}s)'
set log y
set yrange [100:3e4]
plot 'agg3-1.rate_vs.csv' u 1:4:5 title '1 Solver' w yerrorbars,\
     'agg3-3.rate_vs.csv' u 1:4:5 title '3 Solvers' w yerrorbars,\
     'agg3-6.rate_vs.csv' u 1:4:5 title '6 Solvers' w yerrorbars,\
     'agg3-9.rate_vs.csv' u 1:4:5 title '9 Solvers' w yerrorbars
unset log y

