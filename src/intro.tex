\chapter{Introduction and Problem Definition}
%``A wireless sensor network (WSN) of spatially distributed autonomous sensors to
%monitor physical or environmental conditions, such as temperature, sound,
%pressure, etc. and to cooperatively pass their data through the network to a
%main location.''~\cite{wikipedia2014wsn}.
A wireless sensor network (WSN) is a collection of small autonomous computing
devices capable of communication over a wireless network and deployed with the
goal of capturing details about the environment in which they are placed. 
In recent years as the cost of computer components decreases and wireless
capabilities improve, the development of small, low-cost wireless sensors has
increased tremendously~\cite{kahn1999next}.  In just the last 5 years, the rise
of the smart phone as a common personal accessory has transformed human beings
into walking sensor platforms.  When these devices are connected via wireless
computer networks, it becomes possible to obtain information about physical
surroundings on scales never before possible.

Traditional sensor networks include seismometers, weather stations, and even
astronomers comparing readings of the night sky.  The development of global
communication networks has enabled many of these devices to be connected in
large-scale collaborative data gathering tasks.  Combined with the recent
development of low-cost commodity wireless communication networks like Wi-Fi
and Z-Wave, it is now possible for sensors to be deployed in great numbers over
large areas to collect unprecedented quanitites of information.

Coincident with the development of wireless sensor hardware is the attendant
development of software to control, manage, and coordinate these
devices~\cite{levis2005tinyos,akyildiz2005wireless,akyildiz2002wireless}.
These are necessary goals, since WSNs have neither the large computing and
energy resources of traditional computers nor the relatively low numbers of
such computers.  New techniques are being developed to support
self-localization, self-organization, network routing, in-network processing,
and even energy harvesting and sharing among devices~\cite{gurakan2012energy}.
Just as important as managing the networks themselves is how to manage they
data they create once it exits the network.

Many of these systems, however, address only a specific set of goals or
applications.  They focus on an initial goal that needs to be met, and
the resulting design is one that may be incapable of supporting applications
with very different data types, functional requirements, and scales.
%Systems designed to address a single problem are rarely capable
%of adapting over time to support significantly different goals.  
For example, a system designed to gather weather data around the globe will
have different priorities, processes, and data structures than one designed to
control the appliances inside a person's home.  In order to build generalized,
effective systems for managing and manipulating WSN data, it is necessary to
assume that today's hardware and motivations will not be tomorrow's.
\textit{To build such systems, a top-down approach focused on lowering barriers
to entry and ease of development, results in a more flexible design than those
centered around specific hardware devices or network topologies.}

% Need to compare a bit more to alternate paradigms.
% Mesh networks, hierarchical sensor networks, databases built into the OS
% HomeOS, monitoring systems like Nagios
% where is the data, how is it presented, how is it queried?

%\section{Thesis Statement}
%To build smart building systems, following a top-down approach, focused on 
%lowering barriers to entry and ease of use, provides a larger return on 
%investment than bottom-up approaches centered on hardware.

\section{Contribution}
%A set of programming and design abstractions, technology layers, and network 
%protocols for enabling these systems, focusing on \emph{generality}, 
%\emph{scalability}, \emph{ease of use}, and \emph{ease of programming}. An 
%example implementation is provided and evaluated as both a proof of concept and
%an evaluation of the design.

Owl Platform is a multi-layered WSN data management architecture focused on
simple interfaces, flexible data structures, and data re-use.  From its
beginnings as a research tool used to explore wireless localization algorithms,
it has expanded to support a wide range of goals and applications. The goal of
this thesis is to describe Owl Platform as an architecture for manipulating WSN
data and application development, evaluate the performance of a prototype
implementation, and describe the experiences of its developers and users.

Chapter~\ref{sec:architecture} describes the origins of Owl Platform and its
growth over time to meet the needs of its users. Motivated by an indoor
localization system that had outgrown its original design, its design is guided
by three basic assumptions:  Wireless Sensor Networks are wireless, they
produce data, and some data is only useful for a limited period of time.

The system is broken up into five layers with WSNs on the bottom and
Applications on the top.  Interacting with the WSNs are one or more
Aggregators, components that consolidate and filter the WSN data stream.
Aggregators provide components in the Analysis Layer, called Solvers, with an
interface that allows the selection of specific physical hardware types,
specific transmitters, and data flow rates.  As Solvers translate and transform
the WSN data into a virtual representation of the environment, that data is
stored in a World Model.  The World Model provides access to this virtual
representation via set of Requests which can be used by Applications to present
the data to users, or by Solvers to provide more complex analyses of the data
by combining it with external services.

Chapter~\ref{sec:related} describes the systems and products that have
motivated and guided the development of Owl Platform. Infrared and ultrasonic
badges worn by researchers set the stage for systems that provide automated
monitoring and context-based services to the people that use them.  More recent
work on ``smart'' homes and offices describe the types of applications that are
possible by exploiting environments rich with sensors and networked devices.
Finally, the scale of global sensor networks utilizing the framework of the
World Wide Web to provide interoperability demonstrates the effectiveness of
tools which are not overly specialized.

Chapter~\ref{sec:generality} focuses on the need to avoid making too many
assumptions when dealing with WSN data.  Some sensors can be queried and
reprogrammed while others blindly report their readings until their power
sources are exhausted.  The design choices and thinking behind them are
explored and described.  While Owl Platform is intended to be a sort of ``jack
of all trades'' among WSN data management systems, it is a specialist of none.
The advantages and limitations of this approach are presented and justified.

%In Chapter~\ref{sec:generality}, the data types and structures in Owl Platform
%are motivated by the minimalist assumptions proposed earlier. The design
%choices of the system are presented and explained in detail, and the goals
%of the different layer interfaces are detailed.  

Chapters~\ref{sec:aggregator-eval} and \ref{sec:worldmodel-eval} explore the
performance of the two primary components of the Owl Platform's architecture:
the Aggregator and the World Model.  Chapter~\ref{sec:aggregator-eval} studies
the effect on Aggregator performance as a system scales in various dimensions.
It examines the effect of an increasing volume of sensor data arriving from
WSNs, how the number of Solvers requesting that data impacts processing time,
and how the complexity of Solver requests for WSN data has the potential to
create bottlenecks in the system.  It demonstrates how the system is capable
of handling workloads comparable to hundreds of thousands of sensors, and also
discovers some shortcomings in the current prototypes.

Chapter~\ref{sec:worldmodel-eval} looks at two different choices for the World
Model storage engine, and how that choice impacts performance.  It also takes
the first steps in profiling the performance of the World Model as the amount
of stored data increases over time.  Each of the World Model's interfaces are
evaluated and benchmarking measurements are made to use as guideposts for
future improvements.  A discussion of the different prototype implementations'
limitations and possible improvements concludes the evaluation.

Chapter~\ref{sec:experience} describes the different ways that Owl Platform has
been used since its creation.  The various applications that have been
developed on top of Owl Platform are described, and the experiences of their
authors are summarized.  Undergraduate students have built a range of
applications including a stolen chair detector, a context-aware power switch,
and reminders to keep windows closed during cold weather.  Researchers have
built radio tomography systems, augmented reality applications, and conducted
high-density wireless experiments using Owl Platform as a foundation.  A
newly-developed commercial product highlights the robustness and reliability of
a system already proven in the laboratory.

Finally, Chapter~\ref{sec:conclusions} wraps things up and looks at possible
future directions for Owl Platform.  A combination of lessons learned and
new ideas, it plots a course for an improved system that provides greater
performance, richer features, and more flexible interfaces.  
