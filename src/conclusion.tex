\chapter{Conclusions}
\label{sec:conclusions}
Wireless Sensor Networks are complex systems that are only beginning to be
utilized outside of the research laboratory.  As the availability of wireless
sensors increases, costs decrease, and they become a ubiquitous part of an
increasingly connected world, the need to manage and manipulate their data will
only grow. It is very likely that most of these networks will be deployed for a
specific purpose including environmental monitoring, building security,
personal health and wellness.  The systems that integrate these networks' data,
however, need not be so application-specific.  Owl Platform has demonstrated
that a general-purpose system that trades the convenience of built-in
functionality for the flexibility of a system that can grow and evolve over
time.  The basic components of Owl Platform have also demonstrated that they
can perform effectively outside of the constraints of the research laboratory.

%Owl Platform was designed with the goal of supporting wireless sensor data
%from tens of thousands of sensors. It achieved this goal.
From the start, Owl Platform was designed to support Wireless Sensor Networks
on the scale of a large University campus.  Experimental results have shown
that it is capable of accepting thousands of data Samples per second, analogous
to hundreds of thousands of sensors.  Given the explosive recent growth in both
wireless networked devices and sensors, it is necessary that any systems
designed today permit WSNs to grow by several orders of magnitude.  Systems
dedicated solely to a single building or room are giving way to smart power
grids, smart cities, and heterogeneous collections of sensors producing
overwhelming amounts of data.  Modern automobiles contain hundreds of onboard
sensors from the windshield wipers to the tires.  Ensuring high-speed,
low-latency access to this data on a large scale can enable safer roads, more
efficient cities, and more productive communities.  Although there is plenty of
room for improvement, this prototype of Owl Platform has demonstrated its
ability to handle large amounts of sensor data with capacity for additional
expansion and growth. 

%Owl Platform has been adapted to a range of applications: localization,
%building monitoring, smart spaces.
This same explosive growth in the kind and quantity of sensors also means that
the data management systems need to remain flexible as well as fast.  Not
simply focused on processing raw data directly from the sensors, the variety of
Solvers and Applications developed so far hint at a system that is generic and
adaptable enough to adapt to the needs of its users.  The World Model exposes
APIs that translate well to service like localization, mobility detection,
building and environmental monitoring, and the creation of smart spaces. Integration
with external services like weather and traffic reports produces richer applications
than those that are restricted to sensor data.

%Owl Platform's APIs and libraries make it accessible enough that even novice
%programmers are capable of developing applications in short time scales.
With easy to understand libraries and interfaces, it allows even novice
programmers to start developing cutting-edge applications in just a few weeks.
Encouraging analysis by Solvers to focus on specialized, dedicated tasks
ensures that the complexity of the system lies in the network of interactions
between the components, not in the individual processes.  Building dynamic,
complex systems out of simple, manageable components allows better maintenance,
fewer mistakes, and more efficient execution.  

% Modular/layered design allows incremental improvements of individual components
% without significant changes to others.
A multi-layered, modular system provides the opportunity to improve individual
components without significant impact on others.  As long as components
adhere to the contracts required of their interface bindings, implementations
can change and improve incrementally with minimal side effects.  As demonstrated
by the World Model evaluations, improvements in the underlying systems are
necessary for the system to support the scales desired for long periods of time.
These changes remain invisible to the other components, ensuring that the costs
of such improvements remain.

\section{Future Work}
Although Owl Platform has demonstrated that is has great potential, it remains
a work in progress.  During the evaluation of the Aggregator and World Model, 
a number of flaws and limitations have become visible that need to be addressed.
Some of the needed improvements are readily identifiable and easy to achieve while
others require careful consideration and further research.

Evaluation of the Aggregator showed that its performance is currently limited
by the cost of evaluating Subscription Rules for each Sample.  Anecdotal
evidence indicates that caching the results may alleviate some of these costs,
but there is a limit to what caching can achieve.  Careful thought needs to be
given to how best to reduce the number of instructions executed for each
Sample.  For example, the current version relies on the Java Virtual Machine
for garbage collection and memory management.  By reducing the number of
objects needing garbage collection (via object pooling and reuse), the
processor time and memory spent can be reduced and reallocated to other tasks.
This is a common optimization for Java-based appliations, and should be
achievable within a couple of weeks by an experienced programmer.

The choice of programming language may also be a severely limiting factor for
the Aggregator.  Porting part or all of the code into a language like C++ may
also improve efficiency by exploiting the hosts's hardware capabilities.  For
instance, Transmitter ID filtering could be offloaded to a Graphical Processing
Unit (GPU).  The rule checking and filtering can be modeled as a series of data
transforms and mapped to modules in the GPU.  A change like this requires
significant planning and careful design to be effective.  By working together
with a researchers specializing in GPU-based computation, it may be possible to
achieve significant improvements in performance over weeks or months.  A hybrid
approach might be warranted as well, with the bulk of the Aggregator logic
implemented in a portable language like Java to make maintenance and
development easier.  Performance-critical sections can be modularized with
optimized versions written in C++ for GPUs and ``fallback'' versions available
in Java for when appropriate drivers or supported hardware aren't available.

Aside from need for raw speed in the Aggregator, there is strong motivation for
richer feedback to the WSNs producing data.  Even if vast improvements are made
in efficiency, the Aggregator may still reach a point where it can no longer
handle the high volume of Samples being received.  Much like TCP's flow control
mechanism, the Aggregator should inform the WSN gateway when it is overwhelmed
with data.  What form this notification takes, however, is not immediately clear.
A simple approach could have the Aggregator send a value between zero and one
that indicates its estimation of the current capacity.  When it can no longer
keep up with arriving data, it could further indicate the fraction of Samples
that it is shedding in order to continue operating.

More complex feedback could include information about what Transmitters the
Solvers are requesting, and at what inter-arrival rate.  WSNs could then 
make better decisions about how to send what data, but it also begins to weaken
the isolation that is created by a layered approach.  This weakening may be
worthwhile as a compromise, and the different trade-offs of various approaches
need to be studied carefully.

It also appears that while the initial design of the Subscription Request Rules
is sufficient for many applications, there are situations when filtering based
on Receiver IDs are very beneficial. For instance, the student that implemented
the bike rental inventory system relied on proximity to a specific Receiver to
know when a bicycle was returned to the storage area.  If the Aggregator were
receiving data from hundreds of Receivers, and only a handful are important to
a Solver, then it stands to reason that the Aggregator should not forward data
from the other Receivers.  Not only does it place additional burden on the
Aggregator and Solver, but it also generates unnecessary network traffic.

The World Model, too, is not without its need for improvement.  One of the
first observations made while evaluating the network protocols for the World
Model was that the lack of acknowledgement (ACK) messages for Updates,
Expirations, and Deletes meant that there was no way to ensure that an
operation had completed.  One of the reasons it was omitted from the original
protocols, was because there were almost no guarantees about the properties of
operations made by the World Model.  It was originally envisioned as a ``best
effort'' service, but that is no longer sufficient to support the types of
applications desired.  The changes to be made are somewhat small, but would not
be backward compatible.  Any Solvers or Applications using older versions of
the protocol (or libraries) would be required to upgrade in order to
communicate with a World Model using the improved protocol.  Alternatively, it
may be possible for the World Model to support both protocols, but this
introduces greater costs for maintenance any future changes.

The possibility for additional features in the World Model must also be
considered.  In particular, the ability to delete a subset of Attributes values
rather than an entire history, Range Requests with time granularity, access
control, and data secrecy and authenticity guarantees.  

At present, the World Model is only capable of Deleting all values for a
particular Attribute.  The ability to specify exactly which values get deleted,
either based on time ranges or the value itself, would allow finer-grained
control for system administration.  This would eliminate the need to access the
storage engine directly when these functions are required. 

One shortcoming noted when developing web-based applications using the World
Model, is that Range Requests are only able to limit the size of returned data
sets based on time.  In most cases, a requesting Application may have no idea
how much data is contained in the World Model between two timestamp values.  If
too much data is returned, the requestor may become overwhelmed and unable to
process all of the data efficiently.  If too little, the requestor made need to
make multiple requests, wasting time and network resources.  The ability to
limit Range Requests either by size (in bytes) or by number of entries
(Attribute values) could provide significant benefit to systems resources need
to be carefully allocated.  This is another significant change to the World
Model interface, and should be evaluated thoroughly before any change is made.

Access control is a topic that has been discussed by the developers and
researchers since the original implementation was completed. Currently, the
only access control in the World Model is based on the Origin value, which can
easily be forged.  A combination of authentication and authorization mechanisms
need to be developed to ensure that administrative policies for data access and
manipulation can be enforced.  Initial discussions have proposed using a
traditional user-role-privilege model.  User accounts are authenticated at the
beginning of a connection.  Each user is assigned one or more roles, and each
role is assigned one or more privileges.  This is a fairly common system
design, and it may work well for the World Model.  However, careful
consideration is needed before choices are made, since adding any type of
access control mechanism must increase the complexity of the interaction with
the World Model.

In line with access control is the need for data integrity and secrecy, both
which can be provided by public key cryptographic systems.  The Origin 
field of Attribute values can serve a dual purpose in this case.  Data
can be signed by a private key, with the corresponding public key used
as the Origin value. Likewise, data can be encrypted with a public key 
and only decrypted with the corresponding private key.  This would allow,
for instance, a Solver to generate data and encrypt it in such a way that only
an authorized key or set of keys could decrypt it.  Because Client requests
are made by searching Identifiers and Attribute Names, and not the values, 
clients would be able to retrieve the data and decrypt it outside of the
World Model.  The problem of key distribution and signing remain an open
problem, but these are problems encountered by any system using public key
cryptography. A separate entry for public key signatures and certificates
would allow clients to validate data signatures in a similar manner to how
web browsers validate connections to HTTP servers.

A greater understanding of the World Model's performance under different
workloads also needs to be performed.  The basic set of experiments described
in this thesis are a starting point, but do not reflect realistic workloads.
In particular, the impact of large numbers of simultaneous connections needs to be
carefully determined.  Because Owl Platform relies so much on the flow of data
between Solvers and the World Model, even the slightest inefficiencies in this
relationship can be severely limit efficiency when scaled to dozens or hundreds
simultaneous connections.  

Along the same lines, an exploration of the possibility of exploiting the
distributed features of MySQL and MongoDB may provide significant benefits for
the World Model.  Both systems are capable of replication and parallelization,
particularly when it comes to the read-only access of the World Model Client
interface.  This is another significant undertaking, likely requiring months of
effort to design, develop, and evaluate.  However, the potential for the World
Model to support even greater numbers of connections and data rates makes such
an investigation worthwhile.

Across all layers of the system, transport layer security is a critical feature
that is currently absent.  It is now general knowledge the national governments
are capable of intercepting and recording vast amounts of data that transit
public networks.  In many cases sensor data can be inherently private, and
reasonable steps should be taken to ensure it is not intercepted by
unauthorized parties.  The incorporation of end-to-end encryption has been a
planned feature from the start, but has never been a high priority because the
focus has been primarily on research projects.  The recent transition to a
commercial product, however, should motivate the addition of some type of
encryption for the data connections, even if features like access control and
data authentication are postponed.  Although it would require modifying all of
the existing protocol libraries, and both the Aggregator and World Modmel, it
would not necessarily require changes to Applications and Solvers.  Much like
HTTP servers that provide both secure and non-secure interfaces, these
components could also provide encrypted and unencrypted interfaces.  With the
adoption of access control techniques, policy decisions could be enforced which
restrict what data or operations can be performed on unencrypted connections.

