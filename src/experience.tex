\chapter{Experiences in Programming and Use}
\label{sec:experience}
Owl Platform began as a research project designed to make management and
utilization of Wireless Sensor Network data easier.  Over several years, it has
been used in a number of different ways by different groups.  In some cases, it
has been used to speed-up data collection for experimernts, or to combine
multiple systems for analysis. From its earliest stages, it has been used by
the development team, other graduate students, and even undergraduate students
for various projects.  Most recently, it has become one of the core technology
platforms used at a start-up company developing a wireless sensor product.
Whenever it was used for these projects, new insights into both its strengths 
and limitations were found.  In fact, transforming the original World
Server/Distributor model into a single World Model component was strongly
motivated by the experiences of an undergraduate using it just a few months
after the first prototypes were completed.  Overall, it has demonstrated itself
to be a flexible, robust system for collecting, analyzing, and disseminating
WSN data in support of many differing goals.

\section{Research Tools}
As the GRAIL RTLS changed into the Owl Platform, the first applications of the
new system were focused on reproducing previous work.  Developing, deploying,
and testing this new system meant that the previous system was left largely
unattended, and in some cases dismantled to reassign the hardware for Owl
Platform. Consequently, the need to restore a functioning system for the
research group was paramount, and the earliest Solvers and Applications written
for Owl Platform were previously extant services, extensions of those, or
previously-planned ideas.

One of the first applications to be ported into the new Owl Platform was
device-based mobility and localization.  The subject of GRAIL version 2 and
several years of research, it was highly desired to have a functioning indoor
localization system operating again to support research activity.  The result
of this effort was porting the bayesian localization solver such that it
utilized the new network protocols of the Owl Platform rather than the
file-based interfaces it relied on in GRAIL version 2.  A new
mobility-detection solver was also developed and used to initiate
localization of devices if they were found to be moving.

\begin{figure}[hpbt]
  \centering
  \includegraphics[width=0.8\linewidth]{../img/Map_screenshot}
  \caption{Screenshot of the original Flash-based map application.}
  \label{fig:map}
\end{figure}

Equally practical and motivated by research, the localization and mobility
components were completed, but without a usable interface for human users.  An
Adobe Flash\circledR-based map interface (Figure~\ref{fig:map})was written in a couple of months and
made available to the entire research lab where the work was undertaken.  Users
could navigate to a publicly-accessible website and view the location of sensor
tags.  To further motivate users to visit the page, and to solicit feedback,
Pipsqueak sensors were attached to volunteers' coffee cups so that they could
be tracked down if misplaced.  A further benefit of this approach was the
ability for faculty and students to locate specific individuals within the
laboratory space, using the tracked coffee cups as a proxy for the users's
location~\cite{moore2013teatime}.

In the following months, the same map interface was updated to include support
for WSN data generated by the sensors.  New prototype hardware versions were
developed that sensed a variety of physical events including temperature, doors
opening and closing, power outlets, and the office coffee
maker~\cite{firner2011smart}.  The system was also used to support further
development of the Pipsqueak sensors by enabling rapid data collection and
analysis.  Forwarding data from deployed Pipsqueak WSNs enabled both real-time
and post-event analysis to understand the physical signal propagation
properties and frame collision rates under various configurations.

During the course of these development efforts, it was found that much of the
data generated by Solvers could be re-used by other ``upstream'' Solvers and
applications.  For instance, RSSI mean and variance data was used to support
mobility detection, indoor localization, device-free motion
localization~\cite{moore2010geometric}, and passive
localization~\cite{xu2013scpl}.  Other Solvers and Applications were built to
detect and announce freshly-brewed coffee.  The ability to reuse data among
Solvers and Applications, a design decision from the beginning, made
development faster and more efficient.  For instance, when a programming error
resulted in a miscomputed value (e.g.  in RSSI variance values), the error
needed to be corrected only once in a single Solver rather than in multiple
projects where the variance was used as an input value.  In another instance, a
demonstration application was developed and made ready for the 2012 New York
Maker Faire in just 3 days.

External services were  also able to be integrated rapidly into the system by
exploiting the generalized nature of the data stored in the World Model. In one
case, office printers were made accessible to the laboratory staff and students
using a web-based map of the office layout.  A user would click on the printer
icon, select a document, and click a button to print the document.  This was
particularly helpful for users who did not have the proper printer drivers
installed on their computers or mobile phones.

A separate research group in the same laboratory also utilized Owl Platform to
aid in the development and deployment of an augmented reality
headset~\cite{ashok2013bifocus}.  In this case, the researchers modified a 
Pipsqueak tag to enable tight synchronization with infrared LEDs that provided
an accurate way for users to identify their position, direction, and some
nearby objects. Strong motivators for this group to choose the Owl Platform was
its support for the low-power sensors, low latency of sensor data, and Java
libraries that supported rapid development on the Android operating system.


\section{Undergraduate Projects}
Six different undergraduate students utilized the Owl Platform to complete
various sensor-related programming tasks, including complex event detection,
integrating non-sensor data sources, a new World Model storage engine, and an
asset management system.  In each case, the student was given a general
overview of the Owl Platform, its goals and general structure, and access to
publicly-available documentation.  Students were either provided with a set of
initial tasks to select from and attempt to complete.  Where more than one
project was completed, they were the result of the student's desire to move in
a particular direction with their work.  Students were generally allowed to
work independently from the research group, except when they would ask specific
questions about the architecture or library code usage.

% Sumedh Sawant: 6/2011 - 8/2011
\subsection{Student 1}
With the World Server and Distributor still existing as discrete components,
not yet combined into the eventual World Model, a summer intern was recruited
to implement a number of applications using the fledgling Owl Platform.  The
student was given an introduction to the Owl Platform design, was shown the
location of technical documents describing both the network protocols and the
data types in use, and provided a set of Java-based example applications to use
as references.  Over a period of 3 months, the student developed 5 different
applications using a combination of Pipsqueak sensors, external services, and
the Owl Platform services.

The first project was targeted as an introduction to both the sensors and the
software the student would encounter.  In this project, the student attempted
to detect whenever the office refrigerator was left open for a long period of
time which could cause food spoilage and wasted electricity.  Designed and
built in three stages, the student completed the project in approximately two
weeks.  In stage one, the student wrote a Solver that received RSSI values from
the Distributor for a target sensor placed in the refrigerator.  When the
sensor RSSI value was ``low'' for a several seconds, the door was declared
closed.  Likewise, if the RSSI value was ``high'' for several seconds, the door
was declared open.  The Solver updated the Distributor with a new data type
that indicated at what time the door was opened or closed.  The second stage
was an improvement on the first and produced a histogram of RSSI values over a
sliding window.  The Solver would analyze the histogram values, and declare the
refrigerator door to be open or closed depending on a change in median values
over time.  The third stage of the project was a Solver that used the output of
the histogram-based Solver to produce a ``door ajar'' value in the Distributor
if the refrigerator door had been left open for longer than a specified period
of time.  The result of this Solver would be used in the next project, a
notification interface for users to receive real-time updates for events.

\clearpage

\begin{figure}[hpbt]
  \centering
  \includegraphics[width=0.8\linewidth]{../img/Coffeetwitter}
  \caption[Screenshot of Twitter-based notification service]{Example of the Twitter-based coffee notifications by the
  summer intern student.}
  \label{fig:exp_twitter}
\end{figure}

The notification client was also written in Java, and would connect to the
Distributor and World Model and watch for changes to appropriate Attribute
values.  The client also provided a web interface where users could ``sign up''
for email or SMS notifications, and a Twitter account was created which would
host all of the alerts.  The student independently determined how to send
emails from the client, using a Java email library, how to update a Twitter
account programmatically (Figure~\ref{fig:exp_twitter}), and even implemented a
Captcha anti-spam validation form.  Once again, over a period of 2--3 weeks,
the undergraduate intern was able to create a novel application using the Owl
Platform to source data about office activities. 

The third project addressed a problem which was very personally applicable to
the student.  During these summer internships, a large number of incoming
students and interns results in overcrowding in the laboratory space.  The
result of a shortage of work spaces is that chairs often go ``missing'' when
someone leaves their desk for a meeting or lunch.  The student decided to write
a ``stolen chair detector'' which would notify a user whenever a chair
registered to her was moved from its expected location.  The solver reused data
from the localization and mobility solvers to determine where a chair was
located once it had a Pipsqueak sensor attached to it.  The Solver updated a
value in the Distributor which would be used by the notification solver to
alert the ``owner'' whenever it moved a specified distance from its ``resting''
position.

The next Solver written by the student would detect when the daily ``tea time''
was happening in the lab, a social event where students and faculty would
gather in the office kitchen for refreshments and conversation.  On a typical
day, several members of the lab would gather between 3:00 PM and 5:00 PM and
brew fresh coffee and tea.  One particular student was in attendance to most of
these events, and often went around the lab to encourage others to attend.  The
Solver was written such that each day between 3:00 PM and 5:00 PM if at least 3
coffee mugs were located in the kitchen and then coffee was brewed, a tea time
notification would be generated if it had not happened previously that day.
Notifications would be sent by the notification solver to users that signed-up
previously to receive them.  

The final Solver written by the student utilized two different external
services to retrieve traffic problems for a specific geographic area.  The
Solver would request a City and State (or ZIP code) from the user, translate
that to a set of GPS coordinates using an external service, and then
periodically poll a second external service for traffic updates using the GPS
coordinates determined earlier.  The resulting traffic events would then be
updated in the Distributor where they could either be used to generate
notifications to users or as inputs to other solvers.  Although it was never
implemented, there was some early interest to use this information with a
route-planning service to alert users when to leave the lab in order to arrive
at scheduled appointments on time.

Over fewer than 12 weeks, an undergraduate student was able to independently
design, program, and test 5 different components using the Owl Platform as a
framework to manage the data.  At the end of his internship, he discussed his
experiences with the Owl Platform research team.  Partly as a result of his
feedback, the Distributor and World Server components were merged to create the
World Model. After the student left his internship at the end of the summer
period, the notification client was used for several months by the office staff
and students to receive notifications of fresh coffee and tea time gatherings.

% Jonathan Chiu: 9/2011 - 5/2012 (Open window alert/bike rental)
\subsection{Student 2}
The second student was an undergraduate student working for an honors program
as part of his graduate requirement.  He worked closely with a computer science
department staff member who was familiar with the Owl Platform system in
general terms, but who did not have notable experience programming or
configuring it.  The student developed two separate applications on top of the
Owl Platform and also using the Pipsqueak sensors: an open window notification
service and an inventory tracker for a campus bike-sharing
program~\cite{chiu2012slade}.  The student estimated that each project took
approximately 20-40 hours to complete (2--4 weeks).

The first project was a notification service that would alert faculty members
if they left their office window open and the weather report indicated low
temperatures over the next 1--2 days. This is actually frequent problem,
particularly around school vacation periods, and has resulted in frozen pipes
and significant damage to university and personal property.  The project
started by attaching Pipsqueak sensors equipped with magnetic contact switches,
like those used in home security systems, to the windows of several offices in
the building.  An Owl Platform instance was already operating in the building
to support research tasks focused around device localization and Wi-Fi traffic
analysis.  The new sensors' data was forwarded to the existing Owl Platform
system, and the student configured his project to use these components.  Once
the sensor data arrived at the Aggregator, the student's Solver would monitor
the window status to track the open and closed status of the window, adding
this information to the World Model representation of the window.  A second
program, a notification application that the student wrote, periodically
contacted an external weather service to determine the upcoming weather
conditions.  If the forecast indicated low (near-freezing) temperatures and a
window was open, the notification app would access an external directory to
identify the office occupant and send an email reminder to close the window.

The second project was a bicycle inventory tracking application for an
on-campus bike-sharing program.  The purpose of the application was to provide
the bike program's staff with real-time and historical information about the
number of bicycles available at specific rental locations.  Pipsqueak sensors
were attached to bicycles and a Pipsqueak receiver was placed near the area
where the bicycles are stored when not in use.  The student wrote a solver that
would monitor the presence of RSSI data from each of the sensors, and use
absent RSSI values as a proxy for the presence of the bicycle in the storage
area.  The World Model was updated with the quantity of bicycles available as
they left or arrived in the storage area.  The student also built a graphical
tool that allowed the bike program staff to view a graph of bike inventory over
time.

As part of the student's honors program requirements, he wrote a paper about
his project and experience working with the Owl Platform.  Overall, he
described the ease with which programs could be written for someone with
limited programming experience.  He also noted how the change from
Distributor/World Server to World Model architecture caused additional changes
in his projects, but also eased development afterward by introducing simpler
application programming interface (API) tools.  His greatest criticism was on
the lack of complete documentation for the project, a frequent failing of
research projects, and the additional time he needed to understand some
features of the interfaces to successfully complete his projects.

% John Esmet: 1/2012 - 5/2012 (MongoDB World Model)
\subsection{Student 3} The third student ported the World Model from the SQLite
storage engine to MongoDB, a key-value document storage engine.  Over a period
of 5 months, the student worked to design, develop, test, and document the new
storage engine and its performance compared to the SQLite version in use at
that time~\cite{esmet2012mongodb}.

\begin{table}[hpbt]
  \centering
  \begin{tabular}{|l|r|r|}
    \hline
    \textbf{Evaluated Task} & \textbf{SQLite Time} & \textbf{MongoDB Time} \\ \hline
    Insert 900,000 Attribute values & 1,899 s & 380 s \\ \hline
    Snapshot & 589--636 ms & 8--89 ms \\ \hline
    Range Request (400 Attributes) & 29,798 ms & 107 ms \\ \hline
    Range Request (4,500 Attributes) & 34,424 ms & 415 ms \\ \hline
    Range Request (44,000 Attributes) & 37,285 ms & 1,300 ms \\ \hline
  \end{tabular}
  \caption{Comparison of SQLite and MongoDB World Model implementations}
  \label{tab:wm_exp_mongodb_res}
\end{table}

The first task the student completed was understanding the source code of the
SQLite World Model, and how to configure and interact with a MongoDB server
installation using the C++ development library.  Over a period of 2-3 months,
the student completed a functionally-equivalent World Model server that used a
single MongoDB server as the back-end storage engine.  Interestingly, the
student originally attempted to write the entire World Model ``from scratch''
until he realized that a significant portion of the functionality was separate
from the storage engine, and instead decided to re-use the existing code
replace the SQLite code with a compatible interface that utilized MongoDB.  The
student then compared the performance of the two implementations using a set of
repeatable experiments and documented the results, reproduced in
Table~\ref{tab:wm_exp_mongodb_res}.  Overall, it documents that MongoDB is a
strong candidate for use as a World Model storage engine, outperforming the
then-current SQLite version, and likely the MySQL version written later.

The student also made a number of observations both about the SQLite World
Model and the ability for MongoDB to function as an effective storage engine.
One of the most important discoveries was that MongoDB periodically locks
access to the persistent (on-disk) storage device to write all changes to disk.
Unlike a traditional database server, it does not perform this action when a
client requests it and instead performs a set of batched operations.  The
result is the potential to lose the previous minute worth of data in the event
of a server or operating system failure.  The student also noted how much
longer designing and developing such a complex system actually took compared to
his initial estimates.  


% Kang Qin: 1/2012 - 5/2012  (Kinect fall down solver)
\subsection{Student 4} The fourth student developed two Solvers that use the
Microsoft Kinect\circledR camera to capture user motion and determine whether
the person had fallen down or had taken a drink from a glass.  This project was
motivated by a research faculty member pursuing a larger research project for
elder care and aging-in-place techinques.  The student was provided a
programming environment set-up for the camera as well as the requisite
hardware.  Over a period of about 2 months, the student learned to use the
camera and included software development kit (SDK) as well as the Owl Platform
libraries and architecture, build a Solver that monitored a user in view of the
camera, and completed development of two Solvers~\cite{qin2012kinect}.

In order to integrate the camera's output into the Owl Platform, the student
first wrote a simple program that would simply update an on-demand data type in
the World Model with the current data from the camera. Generally, this resulted
in around 15 updates per second, or whatever the output rate of the camera was
configured to do.

The fall detection Solver monitors the position of the subject's head using the
``skeletal'' vertices provided by the camera software.  If the position of the
head drops significantly over approximately one second, it determines that a
fall has taken place.  After this detection, if the subject does not rise
within a few seconds the Solver updates the World Model to indicate that the
user has fallen.  The student noted that this approach suffered from a
relatively high false positive rate, particularly if the subject reached down
to pick something up.

The drinking solver analyzed the position of the subject's hands and head and
tracks the relative distances between them.  If the distance between the head
and one or both hands decreases below a threshold for a short period of time,
the Solver determined that the user had taken a drink from a glass.  The
approach was particularly poor in determining that the user had actually drunk
something, and had not simply placed her hands near her mouth (e.g., eating,
smoking, yawning).  The student suggested, but did not implement, the
integration of mobility data from a cup or other objects that might be used to
supplement the results of the camera-based Solver.

The student demonstrated that the Owl Platform could be used for
non-traditional sensor data, in this case the complex output from a high-speed
camera, and that this data could be integrated into an existing environment
with other data.  He was able to successfully write a program to integrate the
data from the camera as well as two independent Solvers that could be improved
to support elder care efforts in the future.

% Silas and Sai: 6/2012 - 7/2012 (power, standing water)
\subsection{Students 5 and 6} Over 6 weeks, two summer undergraduate intern
students worked to develop both a standing-water sensor and an application to
shut-off appliances when occupants leave a room~\cite{silas2012power}.  The
standing water sensor was a hardware and firmware modification to the Pipsqueak
wireless sensor, and was aided by research faculty to ensure proper
functionality.  The power-control application was developed entirely by the
students.

The standing-water sensor was a modification to the Pipsqueak wireless sensor
that was motivated by the concept of ``smart'' spaces which assist people by
automating the monitoring and control of their living and working areas.  The
sensor modification involved addition of sensor pins and an appropriate
capacitor to ensure that detection would be successful in anything except
distilled water, which does not readily conduct electricity. Initial
modification of the sensor firmware was completed by the students, and minor
changes were made by research faculty to ensure that the sensor remained
low-energy and would operate error-free.  The students then updated an existing
Solver to support decoding the recently-added standing water sensor status
values and add them to the World Model.  The sensor is still actively used by
the research group to support ``smart'' buildings and elder care research.

The power-control project was motivated by a faculty member who frequently left
a portable space heater operating after they left their office.  The unattended
heater wasted electricity and posed a safety hazard, and the faculty member
wished to have an automatic way for it to turn off if left on accidentally.
The students were provided with an IP-enabled power switch, and independently
learned to check its status and control the operation of outlets. They also
developed a Solver that would combine data from two Pipsqueak sensors, one
attached to the office door and one attached to the office chair, with the
power switch.  The solver would monitor the chair and door sensors, and if the
chair was empty and the door was closed for 30 minutes or more, the power
switch would automatically disconnect power to the outlet configured for the
space heater.

Overall, the students were able to modify a hardware sensor, integrate it into
the Owl Platform, and develop a separate application using a third-party power
switch in fewer than 2 months.  They once again helped to demonstrate not only
the versatility provided by the World Model for managing sensor data, but were
also able to quickly understand the core concepts of Owl Platform and build
functional applications with very little assistance from the research team.

\section{Commercial Use}
Owl Platform is currently used as one of the fundamental technologies behind a
commercial laboratory animal care product~\cite{inpoint2014tech}.  After
several months as a prototype trial system, it is currently assisting faculty
and staff by providing continuous monitoring and alerts based on environmental
conditions in multiple animal facilities.  In addition to new
application-specific functionality, its use as a commercial product has also
driven improvements to overall system design, including network protocols, the
World Model, and Aggregator.

Sensors are deployed throughout many facilities with low-power base stations
forwarding data back to an off-site Aggregator.  Solvers decode the sensor
data, generate statistics and provide immediate access to environmental
variables spanning nearly a year.  Staff and faculty at the facility have
expressed great enthusiasm for what they view as a system that deploys rapidly,
is easy to access, and provides low-cost, flexible monitoring.

The application centers around a web-based user interface where staff and faculty
can see real-time alerts and status, analyze data trends across hours or months,
and generate reports for ensuring compliance with regulations and best practices.
Supporting the web interface are real-time, customizable alerts which keep
facility operators aware of any adverse conditions.  Many of these features
were first developed in the laboratory either as student projects, or as
proof-of-concept implementations to demonstrate the power of a general-purpose
framework for WSN data.
