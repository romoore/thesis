\chapter{Related Systems}
\label{sec:related}
The challenges of collecting, organizing, processing, and presenting data about
the physical world have resulted in the proposal and development of many
differing systems.  These include proof-of-concept academic demonstrations,
well-established commercial products, and ad-hoc assemblies of different
commercial products by hobbyists and each has viewed these problems from
slightly different
angles~\cite{gottfried2012twilio,verizon2012homecontrol,adt2013webuserguide,salehi2010gsn}.
Personalized home helpers, enterprise task automation, and global collaboration
all have motivated the development of systems designed to collect, interpret,
and react to data in the real world.  Most, however, are focused on a specific
challenge or domain that they attempt to optimize: from synthesis of inputs from all
over a single home to aggregation of weather and seismic data to assist
researchers.

\section{Vertically Integrated Systems}
Most of the earliest systems built around wireless sensor networks were fully
integrated with the hardware they used for sensing.  As with any technology, it
is reasonable that a ``vertical silo'' is the first modality used in developing
it, especially with research prototypes and proof-of-concept commercial
systems. Until the challenges facing such systems are revealed by the
pioneering technologies, it is not easy to partition them into distinct
components and produce modular systems with specialized components functioning
together.

% 1992 - Active Badge (ORL testing, 1990)
%  IR badge emits 100ms beacon every 15 seconds, sensors pick up beacon
%  Ultrasound not used due to cost consideration
%  6m range, reflects on surfaces in small room, don't go through walls
%  10% tolerance reduces likelyhood of "synchronized" interference
%  Identified that raw data needed to be processed to reduce redundancies
%  4 "sites" (buildings?)
One of the earliest related works in this field is the Active Badge system,
developed in 1990 by Xerox Parc and Olivetti Research
Ltd~\cite{want1992activebadge}.  It focused on a system of infra-red (IR)
``badges'' worn by staff that would periodically beacon an ID to a network of
receivers embedded in the environment.  A central computer then processed these
beacons and produced location information for each badge, and consequently the
staff member who wore the badge.  The system was used by receptionists for
routing calls, colleagues for finding one another, and to keep track of
visitors.  The system was entirely focused on determining the location of staff
and using that data to make day-to-day operations more efficient.  It was even
integrated with the telephone system to automatically route some telephone
calls directly to the handset nearest a person.  

% 2001 - Active Bat
%  "...sensors to update a model of the real world."
%  "Sentient computing" - seems to share users' perception of the world
%  80 different interface types in DB for 1,900 physical objects
Several years later, the Active Bat indoor location system was also developed
in Cambridge in collaboration with one of the original authors of Active Badge
~\cite{addlesee2001batsystem}.  This system was also focused on indoor
location, and utilized ultrasonic beacons and sensors rather than IR to provide
a more precise position estimation.  The result was a system that could do
everything found in its predecessor and more, including automated workstation
configuration, telephone routing within a dense network of cubicles, and
tracking of objects and supplies like personal digital assistants (PDAs).  Like
its predecessor, it employed a nearly monolithic design focused almost entirely
on location data.  It supported many of the same applications as Active Badge
including staff location tracking and automatic telephone call routing.  It
also added new features like audible notification of emails and voicemails, and
repurposing the badge itself to act as a computer mouse on large display
screens.  

Of particular note with Active Bat was its use of a ``world model'' and use of
``...sensors to update a model of the real world.''  This concept, while not
original or unique to the Active Bat systems, was very important, even if it
focused almost entirely on location data.  The combination of the
high-precision badge location data and functional state and location of the
office equipment, made the system much more flexible than its predecessor.
Use of Common Object Request Broker Architecture (CORBA) enabled generalized
modeling and interaction with a number of devices in the system, making it
flexible and extensible for future expansion. 




% 2007 - SenseWeb
%  Built on web services (HTTP/SOAP)
%  "Data transformer" -> solver?
%  "Sensor Gateway" -> interface to sensors/WSN
%  Sensing only, no actuation

% 2008 - SenseWeb
%  Layers: Sensors -> Gateways -> Coordinator -> Transformers -> Apps
%  Location-based indexing of data
%  Focused on GPS/Global data, no discussion of room-local or building-local





%The growing number of networked devices and inexpensive sensors throughout the
%2000's led to the develop of a number of systems designed to integrate that
%data into a common architecture.  
%The focus was to organize, homogenize, and
%combine data from distributed heterogeneous sensors to enable new types of
%application and research.  

The early 2000's saw an increase in the number of low-cost wireless sensor devices
and an increased interest in associated operating systems, networking algorithms,
power management techniques, and administration and data collection
systems~\cite{kahn1999next,akyildiz2002wireless}.  Among these systems, two
dominant techniques for data analysis can be observed: those that task the
network with performing data analysis, and those that analyze the data
externally.  The choice of where data analysis is performed often depends on
the processing, memory, communication resources available to the
network~\cite{romer2004design,yick2008wireless}.

\section{Tightly-Coupled Systems}

Systems that tasked the network itself with the job of efficiently collecting,
organizing, and analyzing sensor data assumed that the sensors had relatively
sophisticated software and hardware resources.  The nodes in these networks
could perform relatively complex operations like resource budgeting, query
optimization, and dynamic network routing.  For instance, Intanagonwiwat
\textit{et al.} proposed a technique for query and data dissemination in
networks where nodes had bidirectional communication and local storage
available~\cite{intanagonwiwat2000directed}.  Likewise, Madden \textit{et al.}
describes a software platform running on an embedded operating system where
nodes work together to execute database-like queries for
data~\cite{madden2005tinydb}.  Using the same operating system, Woo \textit{et
al.} designed a spreadsheet interface that enabled users to reprogram nodes,
issue queries, and process data from sensors~\cite{woo2006spreadsheet}.

% 2008 - SixthSense
%  Infer and track RFID tags in enterprise scenario
%  Focus on people, objects, and their relationships esp. to auto-identify people/objects
%  Use raw DB and processed DB with SQL triggers to process data
%  Experimental set-up described raw DB with 2 updates/second/sensor
%  Relative movement metric - identify which objects move independently, which move only with others
The authors of Active Bat acknowledged that their system would not be
commercially feasible because of the high costs of both the badges and the
large number of sensors needed to be installed for accurate positioning.  The
researchers behind MicroSoft's SixthSense~\cite{ravindranath2008sixthsense},
however, attempted to solve the same problem by using passive RFID badges and
readers spaced throughout an office environment.  Although the cost of RFID
readers is relatively high, at several thousand US dollars per unit, the cost
of the tags themselves is well under one dollar. With this in mind, the authors
proposed and demonstrated a proof-of-concept system that enabled tracking of
individuals and objects, ownership inference, identity inference, and automatic
reservation of conference rooms.  The system is a collection of tightly-coupled
components and provides an API for applications to perform lookups and register
callbacks for targeted events.

With this system, RFID readers are deployed in a non-overlapping pattern
throughout an area of interest.  Each time an RFID tag is read by a reader, the
reading is entered in a ``raw database'' with the tag's identifier, the reader
and antenna, and the timestamp.  Other enterprise data, including user
schedules, keyboard logins, and keycard swipe logs are also entered into this
raw database.  A set of ``inference engines'' then process this raw data to
create information including where a user is, who they are with, and what
devices they are carrying, and place it into a ``processed database''.  For
instance, a series of RFID tag reads over a period of an hour in the raw
database might be transformed into a single entry in the processed database
consisting of a starting and ending timestamp to indicate when the tag was
first and last in the area covered by the same reader.  For outside application
access, an API engine proxies access to the processed database, performing
lookups, caching, and invoking callbacks in applications based on data produced
by the inference engine and requested by the applications.

\section{Loosely-Coupled Systems}

Other systems have also interacted with the sensor nodes, but in a more
abstract and generalized way.  These systems viewed the sensors, or even the
entire network, as having a set of properties and controllers which could be
generalized.  Applications could be built on top of these generalized
interfaces which were capable of exploiting highly heterogeneous networks and
collections of networks.


One system, SenseWeb~\cite{grosky2007senseweb,kansal2008senseweb} utilized the
emerging ubiquity of the World Wide Web, specifically web services, to build a
coordinated system for integrating sensor data.  The system has a simple
layered design centered around a ``coordinator'' that performs a number of
critical tasks including data indexing and archiving, access control, and
coordinated and optimized access to sensors with data needed for multiple
independent applications.  The system is designed to operate in such a way that
sensor owners can enforce access policies to both their sensors and sensor
data.  This enables SenseWeb to interact across multiple administrative domains
without a strong trust in the operator of the common components, including the
Coordinator.

Providing data to the coordinator are a number of ``sensor gateways'', which
transform a device-independent interface, exposing common capabilites.  The
sensor gateway in turn converts the instructions from the coordinator into
device-specific commands, possibly exercising any appropriate policies set by
its owners or operators.  For example, a weather station installed atop an
office building may provide its operator with data every 15 minutes, while only
permitting data to be sent to the public Coordinator every 4 hours.  Likewise,
images captured by a camera may be time-delayed to allow its operators to
redact sensitive images before being made available via the sensor gateway.

With an application programming interface (API) based on web services,
application developers can rapidly develop applications that interact with
SenseWeb by re-using existing HTTP libraries.  This, combined with standardized
set of operations exposed by the Coordinator enables rapid development of many
different applications. Unlike Active Badge and Active Bat, however, SenseWeb
is focused on very large geographic areas: cities, mountain ranges, countries,
or the Earth.  Every sensor is associated with geospatial coordinates, either
manually entered for fixed devices or dynamically generated from GPS satellite
data.  
%Although it supports a large range of values in a common format, it does not
%scale well to indoor environments where GPS locations are hard to acquire
%automatically, and where the necessary calculations are likewise difficult to
%perform by hand.  
Unfortunately, GPS performs consistently poorly when satellite signals are weak,
particularly inside buildings where humans spend the majority of their time.
This type of system, which supports a global range of position values does does
not translate well into indoor spaces like offices and homes.

% 2012 - Home OS
%  "PC-like abstracvtion" for smart home technology
%  Organic, gradual system growth based on users acquiring technology over time
%  Layered architecture
%  Focused only on home, where admin sets all policies
%  Runs on a single host
With similar automation-enhancing goals as SenseWeb, but focused entirely on
the concept of a ``smart home'', a home where sensors and computer systems are
used to enhance and improve the experience of its residents, HomeOS provides an
``operating system'' abstraction to interface with a network of heterogeneous
devices~\cite{dixon2012homeos}. The system is designed to provide non-technical
homeowners and residents the ability to manage sensors, cameras, audio-video
systems, and security systems, combining them with various applicaitons to
produce new functionality.  Examples include light control based on presence
detection and location, music that follows people from room to room, and
automatic notifications to email or mobile telephones.  Composed of 3 main
layers (in addition to the ``application layer''), the system is focused
primarily on management of devices and exposing an abstracted API to
application developers.

Interacting with the applications is the ``Management Layer'', which is
responsible for access control, application installation and configuration,
device installation and configuration, and the user interface.  The Management
layer interacts with a ``Device Functionality Layer'' which uses abstracted
interfaces for devices based on their functionality. For instance, a table lamp
may implement a ``light source'' role which has 3 functions: \textit{on()},
\textit{off()}, and \textit{isOn() \(\to\) Boolean}.  Similarly, a
color-changing LED lamp might also implement a ``colored light source'' role,
which has 2 additional functions: \textit{setColor(Color)} and
\textit{getColor() \(\to\) Color}.  In effect, each device becomes the
composition of the set of roles that it provides to the system. Because each
device may have a different set of interfaces or instructions that it accepts,
the Device Functionality Layer interacts with the Device Connectivity Layer
which implements any device-specific protocols, instructions, and
functionality.

The result is a centralized system that, so long as drivers are available for
the desired devices, enables users to configure and control their home in novel
ways.  Application developers are also able to rapidly develop applications
based on functional roles rather than specific lists of devices or model
numbers.  This means that applications don't need to be updated because new
devices are available, or because a device provides additional roles that were
not previously implemented.

For access management, HomeOS utilizes user accounts and permissions as the two
primary security concepts.  Users are arranged hierarchically into a set of
groups, and each group or user is provided with a set of permissions for an
individual device or group of devices.  Users are arranged in a logical tree
hierarchy, while devices are arranged in a hierarchy by ``rooms''.  A special
high-security device group is used to allow easy grouping and control of
devices considered particularly security-sensitive like cameras or door locks.
When new devices are installed, the system prompts the user to configure access
to the device, for both users and applications, and when new applications are
installed it similarly asks the user to configure access.

\section{Decoupled Systems}

In contrast, others designed systems that agnostically used WSNs as a source of
relatively simple sensor data, and addressed the problem of data collection,
processing, and storage.  These systems rarely made assumptions about the
capabilities of the sensors, and instead assumed that the networks would
produce data over time.  Owl Platform is one such system, and similarly makes
very few assumptions about how the networks will perform.  All of these types
of systems refrain from considering how commands are issued to the network or
how data exits the network, and focus almost entirely on how to process the
data once it is available outside the WSN.

Most examples of this type assume that data processing is handled by a
collection of distributed servers which are primarily tasked with transforming
data from one form or another.  For example, pressure sensor readings on a
roadway might be transformed into information about the number and type of
vehicles passing through an intersection.  Cherniack \textit{et al.} compare
two proposed systems, Aurora* and Medusa, which are primarily concerned with
how to distribute data processing tasks among a set of distributed processing
nodes~\cite{cherniack2003scalable}. Cao \textit{et al.} present and evaluate
an algorithm called RfInfer for distributed processing of radio frequency ID
(RFID) tag location and monitoring~\cite{cao2011distributed}.  Their goal was
to enable computation to be distributed among different processing units while
minimizing the loss of data fidelity caused by incomplete information.

Like these systems, Owl Platform is focused on the management and
transformation of WSN data once it has left the network.  However, instead of
defining a specific application or problem and then setting out to optimize a
solution for that problem, it attempts to define a framework and architecture
that permits many different applications to operate cooperatively on the same
data.  Given the architectural design choices of Owl Platform, it is possible
that systems like Medusa* or RfInfer could operate like Solvers which focus on
specific types of WSN data or application queries.

%\section{Draft Outline}
%\begin{itemize}
%  \item Verizon @Home Verizon Home Monitoring and Control~\cite{verizon2012homecontrol}.
%  \item ADT Security/Monitoring~\cite{adt2014supportdevice},\cite{adt2013webuserguide}
%  \item DIY systems \cite{gottfried2012twilio}
%  \item GSN \cite{salehi2010gsn} \cite{aberer2006gsn}
%\end{itemize}
